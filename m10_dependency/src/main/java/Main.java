import be.kdg.java2.database.HistoricFigureDao;
import be.kdg.java2.database.HistoricFigureDbDao;
import be.kdg.java2.service.HistoricFigureService;
import be.kdg.java2.service.HistoricFigureServiceImpl;
import be.kdg.java2.view.HistoricFigurePresenter;
import be.kdg.java2.view.HistoricFigureView;
import javafx.application.Application;
import javafx.stage.Stage;

import java.util.logging.Logger;

public class Main extends Application {
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());


    public static void main(String[] args) throws Exception {
    /* private static final Logger L = Logger.getLogger(Main.class.getName());*/
        System.out.println("hello");
     }

    @Override
    public void start(Stage primaryStage) throws Exception {
        LOGGER.info("Running start methode on thread: " + Thread.currentThread().getName());
        HistoricFigureDao figureDao = HistoricFigureDbDao.getInstance();
        HistoricFigureView figureView = new HistoricFigureView();
        HistoricFigureService figureService = new HistoricFigureServiceImpl(figureDao);
        new HistoricFigurePresenter(figureService, figureView);

       // primaryStage.setScene(figureView.getScene());
        primaryStage.show();
    }

    /* @Override
     public void start(Stage primaryStage) throws Exception {
      L.info("Running start methode on thread: " + Thread.currentThread().getName());
      SmartphoneDao smartphoneDao = SmartphoneDbDao.getInstance("db/smartphones");
      SmartphonesView smartphonesView = new SmartphonesView();
      SmartphonesService smartphonesService = new SmartphonesServiceImpl(smartphoneDao);
      new SmartphonePresenter(smartphonesView, smartphonesService);

      primaryStage.setScene(new Scene(smartphonesView));
      primaryStage.show();

     }*/
}