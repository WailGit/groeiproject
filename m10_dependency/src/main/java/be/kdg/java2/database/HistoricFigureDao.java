package be.kdg.java2.database;/*
 * Wail Azoukane
 *12/11/2022
 *
 */

import be.kdg.java2.model.HistoricFigure;

import java.sql.SQLException;
import java.util.List;

public interface HistoricFigureDao {

    boolean insert(HistoricFigure figure) throws SQLException;
    boolean delete(String naam);
    boolean update(HistoricFigure figure);
    HistoricFigure retrieve(String naam);
    List<HistoricFigure> sortedOn(String query);
    List<HistoricFigure> getAllFigures();
}
