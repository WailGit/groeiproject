package be.kdg.java2.database;/*
 * Wail Azoukane
 *12/11/2022
 *
 */

import be.kdg.java2.data.Data;
import be.kdg.java2.exceptions.HistoricFigureException;
import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HistoricFigureDbDao implements HistoricFigureDao {
    private static HistoricFigureDbDao INSTANCE;
    private Connection connection;

    private Logger logger = Logger.getLogger("be.kdg.java2.model.HistoricFigure");


    public HistoricFigureDbDao(String databasePath){
        try {
            this.connection = DriverManager.getConnection("jdbc:hsqldb:file:" + databasePath, "sa", "");
            createTable();
        } catch (SQLException throwables){
            throwables.printStackTrace();
        }
    }

    public void close(){
        if(connection != null){
            try{
                connection.close();
            } catch (SQLException throwables){
                throwables.printStackTrace();
            }
        }
    }

    private void createTable() {

            try {
                DatabaseMetaData dbm = connection.getMetaData();
                ResultSet tables = dbm.getTables(null, null, "historicfigures", null);
                if (!tables.next()) {
                    Statement statement = connection.createStatement();
                    statement.execute("DROP TABLE historicfigures IF EXISTS ");
                    String query = "CREATE TABLE historicfigures (" +
                            "id INTEGER IDENTITY ," +
                            "name VARCHAR(30)," +
                            "country VARCHAR(30)," +
                            "height DECIMAL(10,4)," +
                            "kills INTEGER," +
                            "gender VARCHAR(30)," +
                            "birthdate DATE,";
                    statement.execute(query);
                    statement.close();
                    logger.log(Level.INFO, "Table historicfigures created" );

                    logger.info("Table figures created");
                    Data.getData().forEach(this::insert);
                    logger.log(Level.INFO, "Data inserted" );


                }

            } catch (SQLException e) {
                logger.log(Level.SEVERE, "The create table failed " + e.getMessage());
                throw new HistoricFigureException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }

    }


    public static HistoricFigureDbDao getInstance(){
        if (INSTANCE == null){
            return new HistoricFigureDbDao("db/historicfiguresDB");
        }
        return INSTANCE;
    }

    @Override
    public boolean insert(HistoricFigure figure)  {
        try {
            String sqlInsert = "INSERT INTO HISTORICFIGURES (id,surname,country,height,kills,gender,birth) VALUES(?,?,?,?,?,?,?)";
            PreparedStatement prep = connection.prepareStatement(sqlInsert);
            prep.setString(1, null);
            prep.setString(2, figure.getName());
            prep.setString(3, figure.getCountry());
            prep.setDouble(4, figure.getHeight());
            prep.setInt(5, figure.getKills());
            prep.setString(6, figure.getGender().name());
            prep.setString(7, Date.valueOf(figure.getBirthdate()).toString());
            prep.executeUpdate();
            prep.close();
            return true;
        } catch (SQLException e) {


            logger.log(Level.SEVERE, String.format("Can not insert this statement : %s", e.getMessage()));
            return false;
        }
    }

    @Override
    public boolean delete(String naam) {
        try {
            if (naam.equals("*")) {
                Statement statement = connection.createStatement();
                statement.execute("DELETE FROM historicfigures");
                statement.close();
                return true;
            } else {
                String sqlInsert = "DELETE FROM historicfigures WHERE surname=?";
                PreparedStatement prep = connection.prepareStatement(sqlInsert);
                prep.setString(1, naam);
                prep.executeUpdate();
                prep.close();
                return true;
            }

        } catch (SQLException e) {
            logger.log(Level.SEVERE, String.format("Can not delete this : %s", e.getMessage()));
            return false;
        }
    }

    @Override
    public boolean update(HistoricFigure figure) {
        try {
            Statement statement = connection.createStatement();
            String sqlInsert = "UPDATE historicfigures   SET surname =? ,country = ?, height= ?, kills = ?,gender = ?, birth = ? WHERE id= ?";
            PreparedStatement prep = connection.prepareStatement(sqlInsert);
            prep.setString(1, figure.getName());
            prep.setString(2, figure.getCountry());
            prep.setDouble(3, figure.getHeight());
            prep.setInt(4, figure.getKills());
            prep.setString(5, figure.getGender().name());
            prep.setDate(6, Date.valueOf(figure.getBirthdate()));
            prep.setInt(7, figure.getId());
            prep.executeUpdate();
            prep.close();
            return true;


        } catch (SQLException e) {
            logger.log(Level.SEVERE, String.format("Can not update this  : %s", e.getMessage()));
            return false;
        }
    }

    @Override
    public HistoricFigure retrieve(String naam) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM historicfigures WHERE surname = '" + naam + "'");

            while (resultSet.next()) {
                HistoricFigure figure = new HistoricFigure(resultSet.getInt("ID"),resultSet.getString("SURNAME"),
                        resultSet.getString("COUNTRY"), resultSet.getDouble("HEIGHT"),
                        resultSet.getInt("KILLS"), GENDER.valueOf(resultSet.getString("GENDER")),
                        resultSet.getDate("BIRTH").toLocalDate());
                statement.close();
                resultSet.close();
                return figure;
            }
            return null;

        } catch (SQLException e) {
            logger.log(Level.SEVERE, String.format("Can not retrieve this  : %s", e.getMessage()));
            return null;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);

        }
    }

    @Override
    public List<HistoricFigure> sortedOn(String query) {
        try {
            Statement statement = connection.createStatement();
            List<HistoricFigure> myList = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                HistoricFigure figure = new HistoricFigure( resultSet.getInt("id"),resultSet.getString("surname"),
                        resultSet.getString("country"), resultSet.getDouble("height"),
                        resultSet.getInt("kills"), GENDER.valueOf(resultSet.getString("gender")),
                        resultSet.getDate("birth").toLocalDate());
                myList.add(figure);
            }
            statement.close();
            resultSet.close();

            return myList;

        } catch (SQLException e) {
            logger.log(Level.SEVERE, String.format("Can not sort this  : %s", e.getMessage()));
            return null;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<HistoricFigure> getAllFigures() {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM HistoricFigures");
            List<HistoricFigure> figures = new ArrayList<>();
            while (resultSet.next()) {
                figures.add(new HistoricFigure(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("country"),
                        resultSet.getDouble("height"),
                        resultSet.getInt("kills"),
                        GENDER.valueOf(resultSet.getString("gender")),
                        resultSet.getDate("birthdate").toLocalDate()
                ));
            }
            statement.close();
            resultSet.close();
            return figures;
        } catch (SQLException throwables) {
            logger.log(Level.SEVERE, "Failed to retrieve all figures " + throwables.getMessage());
            return null;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }





    }
}
