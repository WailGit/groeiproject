package be.kdg.java2.exceptions;/*
 * Wail Azoukane
 *24/12/2022
 *
 */

public class HistoricFigureException extends RuntimeException{
    public HistoricFigureException(){

    }

    public HistoricFigureException(String message){
        super(message);
    }

    public HistoricFigureException(String message, Throwable cause){
        super(message, cause);
    }

    public HistoricFigureException(Throwable cause){
        super(cause);
    }

    public HistoricFigureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace){
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
