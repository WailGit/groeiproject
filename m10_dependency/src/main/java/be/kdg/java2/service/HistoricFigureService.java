package be.kdg.java2.service;/*
 * Wail Azoukane
 *24/12/2022
 *
 */

import be.kdg.java2.model.HistoricFigure;

import java.sql.SQLException;
import java.util.List;

public interface HistoricFigureService {
    public List<HistoricFigure> getAllHistoricFigures();
    public void addHistoricFigure(HistoricFigure figure) throws SQLException;
}
