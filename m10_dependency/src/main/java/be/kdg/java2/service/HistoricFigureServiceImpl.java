package be.kdg.java2.service;/*
 * Wail Azoukane
 *24/12/2022
 *
 */

import be.kdg.java2.database.HistoricFigureDao;
import be.kdg.java2.database.HistoricFigureDbDao;
import be.kdg.java2.model.HistoricFigure;

import java.sql.SQLException;
import java.util.List;

public class HistoricFigureServiceImpl implements HistoricFigureService{

    HistoricFigureDao database;



    public HistoricFigureServiceImpl(HistoricFigureDao database) {
        this.database = database;
    }


    @Override
    public List<HistoricFigure> getAllHistoricFigures() {
        return database.getAllFigures();
    }

    @Override
    public void addHistoricFigure(HistoricFigure figure) throws SQLException {
        database.insert(figure);
    }
}
