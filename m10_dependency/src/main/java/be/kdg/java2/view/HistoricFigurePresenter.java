package be.kdg.java2.view;/*
 * Wail Azoukane
 *24/12/2022
 *
 */

import be.kdg.java2.exceptions.HistoricFigureException;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.service.HistoricFigureService;
import javafx.collections.FXCollections;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.sql.SQLException;

public class HistoricFigurePresenter {

    HistoricFigureService figureService;
    HistoricFigureView figureView;

    public HistoricFigurePresenter(HistoricFigureService figureService , HistoricFigureView figureView) {
        this.figureService = figureService;
        this.figureView = figureView;
        loadFigures();
        addEventHandlers();
    }


    private void addEventHandlers() {
        figureView.getButtonSave().setOnAction(event -> {
            try {
                HistoricFigure figure = new HistoricFigure();
                figure.setName(figureView.getTextFieldName().getText());
                figure.setHeight(Double.parseDouble(figureView.getTextFieldHeight().getText()));
                figure.setBirthdate(figureView.getDatePickerBirthdate().getValue());
                figureService.addHistoricFigure(figure);
            } catch (HistoricFigureException e) {
                Alert alert = new Alert(Alert.AlertType.WARNING, e.getMessage(), ButtonType.OK);
                alert.showAndWait();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            loadFigures();
        });
    }

    private void loadFigures() {
        try {
            figureView.getTableView().setItems(FXCollections.observableList(figureService.getAllHistoricFigures()));
        } catch (HistoricFigureException  | IllegalArgumentException e) {
            Alert alert = new Alert(Alert.AlertType.WARNING, e.getMessage(), ButtonType.OK);
            alert.showAndWait();
        }
    }
}
