package be.kdg.java2.view;/*
 * Wail Azoukane
 *24/12/2022
 *
 */

import be.kdg.java2.model.HistoricFigure;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;



public class HistoricFigureView  {
    private TableView tableView;

    public TableView getTableView() {
        return tableView;
    }

    public TextField getTextFieldId() {
        return textFieldId;
    }

    public TextField getTextFieldName() {
        return textFieldName;
    }

    public TextField getTextFieldCountry() {
        return textFieldCountry;
    }

    public TextField getTextFieldHeight() {
        return textFieldHeight;
    }

    public TextField getTextFieldAmountKills() {
        return textFieldAmountKills;
    }

    public TextField getTextFieldGender() {
        return textFieldGender;
    }

    public TextField getTextFieldBirthdate() {
        return textFieldBirthdate;
    }

    public DatePicker getDatePickerBirthdate() {
        return datePickerBirthdate;
    }

    public Button getButtonSave() {
        return buttonSave;
    }

    private TextField textFieldId;
    private TextField textFieldName;
    private TextField textFieldCountry;

    private TextField textFieldHeight;
    private TextField textFieldAmountKills;
    private TextField textFieldGender;
    private TextField textFieldBirthdate;

    private DatePicker datePickerBirthdate;
    private Button buttonSave;

    public HistoricFigureView(){
        tableView = new TableView<>();
        TableColumn<HistoricFigure, String> column1 = new TableColumn<>("Id");
        column1.setCellValueFactory(new PropertyValueFactory<>("id"));
        TableColumn<HistoricFigure, String> column2 = new TableColumn<>("Name");
        column2.setCellValueFactory(new PropertyValueFactory<>("name"));
        TableColumn<HistoricFigure, String> column3 = new TableColumn<>("Country");
        column2.setCellValueFactory(new PropertyValueFactory<>("country"));
        TableColumn<HistoricFigure, String> column4 = new TableColumn<>("Height");
        column3.setCellValueFactory(new PropertyValueFactory<>("height"));
        TableColumn<HistoricFigure, String> column5 = new TableColumn<>("Kills");
        column4.setCellValueFactory(new PropertyValueFactory<>("kills"));
        TableColumn<HistoricFigure, String> column6 = new TableColumn<>("Gender");
        column5.setCellValueFactory(new PropertyValueFactory<>("gender"));
        TableColumn<HistoricFigure, String> column7 = new TableColumn<>("Birthdate");
        column6.setCellValueFactory(new PropertyValueFactory<>("birthdate"));


        tableView.getColumns().add(column1);
        tableView.getColumns().add(column2);
        tableView.getColumns().add(column3);
        tableView.getColumns().add(column4);
        tableView.getColumns().add(column5);
        tableView.getColumns().add(column6);
        tableView.getColumns().add(column7);

        //setCenter(tableView);
        BorderPane.setMargin(tableView, new javafx.geometry.Insets(10));

        textFieldId = new TextField();
        textFieldId.setPromptText("Id");

        textFieldName = new TextField();
        textFieldName.setPromptText("Name");

        textFieldCountry = new TextField();
        textFieldCountry.setPromptText("Country");

        textFieldHeight = new TextField();
        textFieldHeight.setPromptText("Height");

        textFieldAmountKills = new TextField();
        textFieldAmountKills.setPromptText("Kills");

        textFieldGender = new TextField();
        textFieldGender.setPromptText("Gender");

        textFieldBirthdate = new TextField();
        textFieldBirthdate.setPromptText("Birthdate");

        datePickerBirthdate = new DatePicker();
        datePickerBirthdate.setPromptText("Release date");


        buttonSave = new Button("Save");

        HBox hbox = new HBox(textFieldName, textFieldCountry, textFieldHeight, textFieldAmountKills, textFieldGender, textFieldBirthdate, datePickerBirthdate, buttonSave);
        BorderPane.setMargin(hbox, new javafx.geometry.Insets(10));
        hbox.setSpacing(10);
        //setBottom(hbox);
    }


}
