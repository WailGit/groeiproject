import be.kdg.java2.data.Data;
import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigures;

import java.time.LocalDate;
import java.util.List;

public class Demo1 {
    public static void main(String[] args) throws Exception {


       HistoricFigures figures= new HistoricFigures();
        List<HistoricFigure> dataList= Data.getData();

        for (HistoricFigure figure : dataList) {
            figures.add(figure);
        }

        dataList.forEach(figures::add);

       figures.add(dataList.get(2));



        System.out.println(figures.search("Muhammed Ali")+"\n"); // true
        System.out.println(figures.search("zizou")+"\n"); //false ( null needs to be printed

        System.out.println(figures.getSize()+"\n"); // 15
        figures.remove("Muhammed Ali");
        System.out.println(figures.getSize()+"\n"); // 14

        figures.sortedOnBirth().forEach(System.out::println);

        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        figures.sortedOnHeight().forEach(System.out::println);
        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        figures.sortedOnKills().forEach(System.out::println);


        HistoricFigure tesyNameArgument1 = new HistoricFigure(null, "DUMMY", 1.2, 0, GENDER.Male, LocalDate.now());
        HistoricFigure tesyNameArgument2 = new HistoricFigure("", "DUMMY", 1.2, 0, GENDER.Male, LocalDate.now());

        HistoricFigure tesyCountryArgument1 = new HistoricFigure("DUMMY", null, 1.2, 0, GENDER.Male, LocalDate.now());
        HistoricFigure tesyCountryArgument2 = new HistoricFigure("DUMMY", "", 1.2, 0, GENDER.Male, LocalDate.now());

        HistoricFigure tesyHeightArgument1 = new HistoricFigure("DUMMY", "DUMMY", 0, 0, GENDER.Male, LocalDate.now());
        HistoricFigure tesyHeightArgument2 = new HistoricFigure("DUMMY", "DUMMY", -65645, 0, GENDER.Male, LocalDate.now());

        HistoricFigure tesyKillsArgument = new HistoricFigure("DUMMY", "DUMMY", 1.2, -3, GENDER.Male, LocalDate.now());
        HistoricFigure tesyGenderArgument = new HistoricFigure("DUMMY", "DUMMY", 1.2, 0, null, LocalDate.now());
        HistoricFigure tesyBirthArgument = new HistoricFigure("DUMMY", "DUMMY", 1.2, 0, GENDER.Male, null);




    }
}