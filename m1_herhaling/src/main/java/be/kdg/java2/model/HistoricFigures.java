package be.kdg.java2.model;

import java.util.*;

public class HistoricFigures {


    private static class BirthComparator implements Comparator<HistoricFigure> {


        @Override
        public int compare(HistoricFigure o1, HistoricFigure o2) {
            return o1.getBirthdate().compareTo(o2.getBirthdate());
        }
    }

    private static class HeightComparator implements Comparator<HistoricFigure> {


        @Override
        public int compare(HistoricFigure o1, HistoricFigure o2) {
            return Double.compare(o1.getHeight(),o2.getHeight());
        }
    }

    private static class KillsComparator implements Comparator<HistoricFigure> {


        @Override
        public int compare(HistoricFigure o1, HistoricFigure o2) {
            return Integer.compare(o1.getKills(), o2.getKills());
        }
    }


   private final  TreeSet<HistoricFigure> setOfFigures= new TreeSet<>();


    public boolean add(HistoricFigure figure){
        return setOfFigures.add(figure);
    }


    public boolean remove(String name) throws IllegalAccessException {
        return setOfFigures.removeIf(fig -> fig.getName().equals(name));
    }


    public HistoricFigure search(String name) throws IllegalAccessException {


        // dit kon ook met de contains functie, maar dan kon je niet dat object returnen!
        for(HistoricFigure figure: setOfFigures){
            if(figure.getName().equals(name)){
                return figure;
            }
        }
        return null;
    }


    public List<HistoricFigure> sortedOnHeight(){
        List<HistoricFigure> temp= new ArrayList<>(setOfFigures);
        temp.sort(new HeightComparator());

        return temp;
    }
    public List<HistoricFigure> sortedOnBirth(){
        List<HistoricFigure> temp= new ArrayList<>(setOfFigures);
        temp.sort(new BirthComparator());

        return temp;
    }
    public List<HistoricFigure> sortedOnKills(){
        List<HistoricFigure> temp= new ArrayList<>(setOfFigures);
        temp.sort(new KillsComparator());

        return temp;
    }


    public int getSize(){
        return setOfFigures.size();
    }


}
