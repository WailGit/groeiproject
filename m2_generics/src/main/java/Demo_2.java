import be.kdg.java2.data.Data;
import be.kdg.java2.generics.PriorityQueue;
import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;

public class Demo_2 {

    public static void main(String[] args) throws Exception {
       var myQueue = new PriorityQueue<>();
        myQueue.enqueue("Tokio", 2);
        myQueue.enqueue("Denver", 5);
        myQueue.enqueue("Rio", 2);
        myQueue.enqueue("Oslo", 3);
        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(myQueue.toString());
        System.out.println("aantal: " + myQueue.getSize());
        System.out.println("positie van Tokio: " + myQueue.search("Tokio"));
        System.out.println("positie van Nairobi: " + myQueue.search("Nairobi"));
        for (int i = 0; i < 4; i++) {
            System.out.println("Dequeue: " + myQueue.dequeue());
        }
        System.out.println("Size na dequeue: " + myQueue.getSize());


        var figures =Data.getData();
        var mySecondQueue = new PriorityQueue<>();
        var random = new Random();


        for (HistoricFigure figure : figures) {
            mySecondQueue.enqueue(figure, random.nextInt(5)+1);
        }

        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(mySecondQueue.toString());
        System.out.println("aantal: " + mySecondQueue.getSize());

        HistoricFigure Muhammed = new HistoricFigure("Muhammed Ali","America",1.5,0, GENDER.Male, LocalDate.of(1942,1,17));
        HistoricFigure Fake= new HistoricFigure();
        Fake.setName("Fake");

        System.out.println("positie van Muhammed Ali: " + mySecondQueue.search(Muhammed));
        System.out.println("positie van Fake: " + mySecondQueue.search(Fake));

        for (int i = 0; i < 4; i++) {
            System.out.println("Dequeue: " + mySecondQueue.dequeue());
        }
        System.out.println("Size na dequeue: " + mySecondQueue.getSize());







    }
}