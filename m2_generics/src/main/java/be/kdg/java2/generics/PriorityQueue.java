package be.kdg.java2.generics;

import java.util.*;

public class PriorityQueue<T> implements FIFOQueue<T>{
    private TreeMap<Integer, LinkedList<T>> myMap = new TreeMap<>(Comparator.reverseOrder());


    @Override
    public boolean enqueue(T element, int priority) {
        if(myMap.containsValue(element))return false;

        if(myMap.containsKey(priority)){
            myMap.get(priority).add(element);
        }else{
            myMap.put(priority,new LinkedList<T>());
            myMap.get(priority).add(element);
        }

        return true;

    }

    @Override
    public T dequeue() {
        int highestprior=0;

        for (Integer integer : myMap.keySet()) {
         if(integer>highestprior){
             highestprior=integer;
         }
        }


        T element= myMap.get(highestprior).getFirst();
        myMap.get(highestprior).removeFirst();
        if(myMap.get(highestprior).size()==0){
            myMap.remove(highestprior);
        }
        return element;
    }

    @Override
    public int search(T element) {
        int position=0;
        for (Integer integer : myMap.keySet()) {
            LinkedList<T> tmp= myMap.get(integer);
            for(int i =0;i<tmp.size();i++){
                if(tmp.get(i).equals(element)){
                    position+=i+1;
                    return position;
                }
            }
            position+=tmp.size();
        }

        return -1;
    }

    @Override
    public int getSize() {
        int size=0;
        for (Integer integer : myMap.keySet()) {
           size+= myMap.get(integer).size();
        }
        return size;
    }

    @Override
    public String toString() {
        StringBuilder builder= new StringBuilder();

        for (Integer integer : myMap.keySet()) {

            for(int i = 0; i< myMap.get(integer).size(); i++){
                builder.append(String.format("%d: %s\n",integer, myMap.get(integer).get(i)));
            }
        }

        return builder.toString();
    }
}
