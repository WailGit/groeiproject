package be.kdg.java2.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import static be.kdg.java2.model.GENDER.UNKOWN;


/*
* DummyWaarde voor datum 1/1/1975
* DummyWaarde voor lengte: 1
* */


/**
 * Historic figure saves information of all historic figures such as name,country,height,kills,gender and birthdate
 *
 * @author Wail Azoukane
 * @version 1.1
 * @see  <a href="https://www.smithsonianmag.com/smithsonianmag/meet-100-most-significant-americans-all-time-180953341/#:~:text=By%20their%20reckoning%2C%20Jesus%2C%20Napoleon,five%20figures%20in%20world%20history.">label</a>
 *
 */

public class HistoricFigure implements Comparable<HistoricFigure> {
    private String name;
    private String country;
    private double height;
    private int kills;

    private GENDER gender;
    private LocalDate birthdate;

    /**
     * Maakt een nieuw HistoricFigure object aan met dummy waarde
     *
     * @throws IllegalArgumentException when paramters are not valid
     */
    public HistoricFigure() throws IllegalArgumentException {
        this("Unkown","Unkown",1,0,UNKOWN,LocalDate.of(1975,1,1));
    }

    /**
     *
     * @param name is the name of the person
     * @param country is a country's name where the person has lived
     * @param height is the height of the person
     * @param kills is the amount of people the person has killed
     * @param gender is the persons gender identity
     * @param birthdate is the day of birth of the person
     * @throws IllegalArgumentException when parameters are not valid
     * @see GENDER
     */

    public HistoricFigure(String name, String country, double height, int kills, GENDER gender, LocalDate birthdate) throws IllegalArgumentException {

        try{
            setName(name);
            setCountry(country);
            setBirthdate(birthdate);
            setHeight(height);
            setKills(kills);
            setGender(gender);
        }catch(Exception e){
            System.out.print(e.getMessage());
        }

    }

    /**
     *
     * @return the name of the person as a string
     */

    public String getName() {
        return name;
    }

    /**
     *
     * @return the name of the country where he has lived as a string
     */
    public String getCountry() {
        return country;
    }

    /**
     *
     * @return the height of the person as a double
     */
    public double getHeight() {
        return height;
    }

    /**
     *
     * @return the amount of kills as a integer
     */
    public int getKills() {
        return kills;
    }

    /**
     *
     * @return the gender identity as GENDER enum type
     */
    public GENDER getGender() {
        return gender;
    }

    /**
     *
     * @return the date the person was born as a LocalDate
     */
    public LocalDate getBirthdate() {
        return birthdate;
    }

    /**
     *
     * @param name is the name of a person
     * @throws IllegalArgumentException when name is null or an empty string
     */
    public void setName(String name) throws IllegalArgumentException {
        if(name== null || name.equals("")){
            throw new IllegalArgumentException("Please enter a valid name in!");
        }

        this.name=name;
    }

    /**
     *
     * @param country is a country's name where the person has lived
     * @throws IllegalArgumentException when country is null or an empty string
     */
    public void setCountry(String country) {
        if(country== null || country.equals("")){
            throw new IllegalArgumentException("Please enter a valid coutryname in!");
        }

        this.country = country;
    }

    /**
     *
     * @param height is the height of the person
     * @throws IllegalArgumentException when heihgt<=0
     */

    public void setHeight(double height) {
        if( height<=0){
            throw new IllegalArgumentException("Height can not be negative or 0!");
        }
        this.height = height;
    }

    /**
     *
     * @param kills is the amount of people the person has killed
     * @throws IllegalArgumentException when the parameter kills <0
     */
    public void setKills(int kills) {
        if( kills<0){
            throw new IllegalArgumentException("Amount of kills can not be negative!");
        }
        this.kills = kills;
    }


    /**
     *
     *  @param gender is the persons gender identity
     *
     *  @throws IllegalArgumentException when gender is null
     */
    public void setGender(GENDER gender) {
        if(gender== null){
            throw new IllegalArgumentException("Please choose a gender M/F!");
        }
        this.gender = gender;
    }

    /**
     * @param birthdate is the day of birth of the person
     * @throws IllegalArgumentException when birthdate is null or the year is greater than current date year
     */

    public void setBirthdate(LocalDate birthdate) {
        if (birthdate==null || birthdate.getYear()>LocalDate.now().getYear() ){
            throw new IllegalArgumentException("Please enter a birthdate that is less then the currentyear");
        }
        this.birthdate = birthdate;
    }

    /**
     *
     * @param o is an object
     * @return true if the object is the same, else it turns false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoricFigure figure = (HistoricFigure) o;

        return name.equals(figure.name);
    }

    /**
     *
     * @return a hascode calculated on name
     */
    @Override
    public int hashCode() {
        return name.hashCode();
    }

    /**
     *
     * @param o the object to be compared.
     * @return an integer that is either 1, -1 or 0
     */
    @Override
    public int compareTo(HistoricFigure o) {
        return this.name.compareTo(o.getName());
    }


    /**
     *
     * @return a string of the object formed with the current field values
     */
    @Override
    public String toString() {

        DateTimeFormatter format= DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);

        return String.format("name: %s country:%s height:%.2f kills:%d gender:%s birthdate:%s\n",name,country,height,kills,gender.toString(),format.format(birthdate));

    }
}
