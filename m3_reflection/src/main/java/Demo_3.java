
import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigures;
import be.kdg.java2.model.Person;
import be.kdg.java2.reflection.ReflectionTools;

import java.sql.Ref;
import java.time.LocalDate;
import java.util.Random;

public class Demo_3 {

    public static void main(String[] args) throws Exception {


       ReflectionTools.classAnalysis(Person.class,HistoricFigure.class, HistoricFigures.class);

        System.out.println(ReflectionTools.runAnnotated(HistoricFigure.class));

    }
}