package be.kdg.java2.model;

import be.kdg.java2.reflection.CanRun;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import static be.kdg.java2.model.GENDER.UNKOWN;


/*
* DummyWaarde voor datum 1/1/1975
* DummyWaarde voor lengte: 1
* */


/**
 * Historic figure saves information of all historic figures such as name,country,height,kills,gender and birthdate
 *
 * @author Wail Azoukane
 * @version 1.1
 * @see  <a href="https://www.smithsonianmag.com/smithsonianmag/meet-100-most-significant-americans-all-time-180953341/#:~:text=By%20their%20reckoning%2C%20Jesus%2C%20Napoleon,five%20figures%20in%20world%20history.">label</a>
 */

public class HistoricFigure extends Person  {
    private String country;
    private int kills;

    /**
     * Maakt een nieuw HistoricFigure object aan met dummy waarde
     *
     * @throws IllegalArgumentException when paramters are not valid
     */
    public HistoricFigure() throws IllegalArgumentException {
        this("Unkown","Unkown",1,0,UNKOWN,LocalDate.of(1975,1,1));
    }

    /**
     *
     * @param name is the name of the person
     * @param country is a country's name where the person has lived
     * @param height is the height of the person
     * @param kills is the amount of people the person has killed
     * @param gender is the persons gender identity
     * @param birthdate is the day of birth of the person
     * @throws IllegalArgumentException when parameters are not valid
     */

    public HistoricFigure(String name, String country, double height, int kills, GENDER gender, LocalDate birthdate) throws IllegalArgumentException {
        super(name, height, gender, birthdate);
        try{
            setCountry(country);
            setKills(kills);
        }catch(Exception e){
            System.out.print(e.getMessage());
        }

    }

    /**
     *
     * @return the name of the country where he has lived as a string
     */
    public String getCountry() {
        return country;
    }

    /**
     *
     * @return the amount of kills as a integer
     */
    public int getKills() {
        return kills;
    }

    /**
     *
     * @param country is a country's name where the person has lived
     * @throws IllegalArgumentException when country is null or an empty string
     */

    @CanRun("Belgie")
    public void setCountry(String country) {
        if(country== null || country.equals("")){
            throw new IllegalArgumentException("Please enter a valid coutryname in!");
        }

        this.country = country;
    }

    /**
     *
     * @param kills is the amount of people the person has killed
     * @throws IllegalArgumentException when the parameter kills <0
     */
    public void setKills(int kills) {
        if( kills<0){
            throw new IllegalArgumentException("Amount of kills can not be negative!");
        }
        this.kills = kills;
    }


    /**
     *
     * @return a string of the object formed with the current field values
     */
    @Override
    public String toString() {


        return String.format("%s country:%s kills:%d\n",super.toString(),country,kills);

    }
}
