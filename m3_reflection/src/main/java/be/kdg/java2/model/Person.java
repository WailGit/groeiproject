package be.kdg.java2.model;/*
 * Wail Azoukane
 *5/10/2022
 *
 */

import be.kdg.java2.reflection.CanRun;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class Person implements Comparable<Person>{
    private String name;
    private double height;
    private GENDER gender;
    private LocalDate birthdate;


    public Person(String name, double height, GENDER gender, LocalDate birthdate) {
        try{
           setBirthdate(birthdate);
           setHeight(height);
           setName(name);
           setGender(gender);
        }catch(Exception e){
            System.out.print(e.getMessage());
        }

    }



    /**
     * @return the name of the person as a string
     */

    public String getName() {
        return name;
    }

    /**
     * @return the height of the person as a double
     */
    public double getHeight() {
        return height;
    }

    /**
     * @return the gender identity as GENDER enum type
     */
    public GENDER getGender() {
        return gender;
    }

    /**
     * @return the date the person was born as a LocalDate
     */
    public LocalDate getBirthdate() {
        return birthdate;
    }

    /**
     * @param name is the name of a person
     * @throws IllegalArgumentException when name is null or an empty string
     */
    @CanRun
    public void setName(String name) throws IllegalArgumentException {
        if (name == null || name.equals("")) {
            throw new IllegalArgumentException("Please enter a valid name in!");
        }

        this.name = name;
    }

    /**
     * @param height is the height of the person
     * @throws IllegalArgumentException when heihgt<=0
     */

    public void setHeight(double height) {
        if (height <= 0) {
            throw new IllegalArgumentException("Height can not be negative or 0!");
        }
        this.height = height;
    }

    /**
     * @param gender is the persons gender identity
     * @throws IllegalArgumentException when gender is null
     */
    public void setGender(GENDER gender) {
        if (gender == null) {
            throw new IllegalArgumentException("Please choose a gender M/F!");
        }
        this.gender = gender;
    }

    /**
     * @param birthdate is the day of birth of the person
     * @throws IllegalArgumentException when birthdate is null or the year is greater than current date year
     */

    public void setBirthdate(LocalDate birthdate) {
        if (birthdate == null || birthdate.getYear() > LocalDate.now().getYear()) {
            throw new IllegalArgumentException("Please enter a birthdate that is less then the currentyear");
        }
        this.birthdate = birthdate;
    }

    /**
     * @param o is an object
     * @return true if the object is the same, else it turns false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoricFigure figure = (HistoricFigure) o;

        return name.equals(figure.getName());
    }

    /**
     * @return a hascode calculated on name
     */
    @Override
    public int hashCode() {
        return name.hashCode();
    }

    /**
     * @param o the object to be compared.
     * @return an integer that is either 1, -1 or 0
     */
    public int compareTo(Person o) {
        return this.name.compareTo(o.getName());
    }


    @Override
    public String toString() {

        DateTimeFormatter format= DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);

        return String.format("name: %s height:%.2f gender:%s birthdate:%s\n",name,height,gender.toString(),format.format(birthdate));

    }
}
