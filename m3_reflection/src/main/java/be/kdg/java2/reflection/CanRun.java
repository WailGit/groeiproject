package be.kdg.java2.reflection;/*
 * Wail Azoukane
 *6/10/2022
 *
 */

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface CanRun {

    String value() default "dummy";
}
