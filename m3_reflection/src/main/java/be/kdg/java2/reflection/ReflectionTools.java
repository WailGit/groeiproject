package be.kdg.java2.reflection;/*
 * Wail Azoukane
 *5/10/2022
 *
 */

import com.sun.jdi.InterfaceType;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionTools {


    public static void classAnalysis(Class<?> ... aClass){

        for(int i =0;i< aClass.length;i++){
            Class<?> currentClass = aClass[i];

            System.out.println(String.format("Analyse van de klasse : %s\n===============================================\n",currentClass.getSimpleName()));


            System.out.println(String.format("Fully qualified name: %s\n",currentClass.getName()));
            System.out.println(String.format("Name of the superclass: %s\n",currentClass.getSuperclass().getSimpleName()));
            System.out.println(String.format("Name of the package: %s\n",currentClass.getPackageName()));

            // code om alle interfaces te tonen
            StringBuilder result= new StringBuilder("interfaces: ");
            for(Class<?> inter : currentClass.getInterfaces()){
                result.append(inter.getSimpleName()).append(" ");
            }
            System.out.println(result);


            result.setLength(0);
            result.append("Constructors: ");
            for(Constructor constructor: currentClass.getDeclaredConstructors()){
                result.append(constructor.toGenericString()).append(" ");
            }
            System.out.println(result);
            result.setLength(0);
            result.append("attributen: ");
            for(Field field: currentClass.getDeclaredFields()){
                result.append(field.getName()).append(String.format("(%s) ",field.getType().getSimpleName()));
            }
            System.out.println(result);



            result.setLength(0);
            result.append("getters: ");
            for(Method method: currentClass.getDeclaredMethods()){
                if(method.getName().startsWith("get")) {
                    result.append(method.getName()).append(" ");
                }
            }

            System.out.println(result);





            result.setLength(0);
            result.append("setters: ");
            for(Method method: currentClass.getDeclaredMethods()){
                if(method.getName().startsWith("set")) {
                    result.append(method.getName()).append(" ");
                }
            }

            System.out.println(result);






            result.setLength(0);
            result.append("andere methoden: ");
            for(Method method: currentClass.getDeclaredMethods()){
                if(!(method.getName().startsWith("get")|| method.getName().startsWith("set"))) {
                    result.append(method.getName()).append(" ");
                }
            }

            System.out.println(result+"\n");

        }


    }

    public static Object runAnnotated(Class<?> aClass) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        Object object = aClass.getConstructor().newInstance();
        Class<?> superClass = aClass.getSuperclass();

        for(Method method: aClass.getDeclaredMethods()){
            if(method.isAnnotationPresent(CanRun.class) && method.getGenericParameterTypes()[0].equals(String.class))
            {
                method.invoke(object,method.getAnnotation(CanRun.class).value());
            }
        }

        for(Method method: superClass.getDeclaredMethods()){

            if(method.getAnnotation(CanRun.class)!= null){
                method.invoke(object,method.getAnnotation(CanRun.class).value());
            }
        }

        return object;
    }
}


