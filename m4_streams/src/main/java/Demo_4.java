import be.kdg.java2.data.Data;
import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigures;
import be.kdg.java2.util.HistoricFigureFunctions;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Demo_4 {
    public static void main(String[] args) throws Exception {

        HistoricFigures figures= new HistoricFigures();
        List<HistoricFigure> dataList= Data.getData();
        dataList.forEach(figures::add);

        //

        System.out.println("\nHistoric figures sorted on name:");
        figures.sortedBy(HistoricFigure::getName).forEach(System.out::println);

        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        System.out.println("\nHistoric figures sorted on height:");
        figures.sortedBy(HistoricFigure::getHeight).forEach(System.out::println);
        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        System.out.println("\nHistoric figures sorted on kills:");
        figures.sortedBy(HistoricFigure::getKills).forEach(System.out::println);




        System.out.println("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXX  predicate test    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");

        System.out.println(" kills filter\n");
        HistoricFigureFunctions.filteredList(dataList,t -> t.getKills()>0).forEach(System.out::println);
        System.out.println(" height filter\n");
        HistoricFigureFunctions.filteredList(dataList,t -> t.getHeight()>1.80).forEach(System.out::println);
        System.out.println(" gender filter\n");
        HistoricFigureFunctions.filteredList(dataList,t -> t.getGender()==GENDER.Female).forEach(System.out::println);



        System.out.println("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXX  TodoubleFunction test    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
        System.out.printf("kill avergae: %.2f\n",HistoricFigureFunctions.average(dataList, HistoricFigure::getKills));
        System.out.printf("height avergae:  %.2f\n",HistoricFigureFunctions.average(dataList, HistoricFigure::getHeight));



        System.out.println("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXX  predicate test 2   XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
        System.out.println(HistoricFigureFunctions.countIf(dataList, t -> t.getKills()==0)+"\n");
        System.out.println(HistoricFigureFunctions.countIf(dataList, t -> t.getHeight()>1.85)+"\n");

        System.out.println("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXX  stream test date   XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");

        dataList= Data.getData();
        Predicate<HistoricFigure> datePred =t -> t.getBirthdate().getYear()> 1900;

        long amount=HistoricFigureFunctions.countIf(dataList,datePred);
        System.out.println("Amount of historic figures born after 1900:  "+amount);


        System.out.println("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXX  stream test sorted on kills and name   XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");

        dataList.stream().sorted(Comparator.comparing(HistoricFigure::getName).thenComparing(HistoricFigure::getKills)).forEach(System.out::println);


        System.out.println("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXX  stream test All historic figure names in capital letters  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");

        System.out.println(dataList.stream().map(HistoricFigure::getName).map(String::toUpperCase).distinct().sorted(Comparator.reverseOrder()).collect(Collectors.joining(",")));


        System.out.println("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXX  stream test findy any with kills ==0 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");

        dataList.stream().filter(t -> t.getKills()==0).findAny().ifPresentOrElse(e -> System.out.printf(String.format("%s: %d",e.getName(),e.getKills())),()-> System.out.println("Did not find a historic figure"));


        System.out.println("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXX  stream test champion in certain are XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");

        System.out.print("Champion in kills: ");
        dataList.stream().max(Comparator.comparing(HistoricFigure::getKills)).map(HistoricFigure::getName).ifPresent(System.out::println);
        System.out.print("Champion in tallest heigt: ");
        dataList.stream().max(Comparator.comparing(HistoricFigure::getHeight)).map(HistoricFigure::getName).ifPresent(System.out::println);

        System.out.println("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXX  stream test filter on a string criterium XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");


        List<String> tmp = dataList.stream().map(HistoricFigure::getName).filter(t -> t.startsWith("M")).sorted().collect(Collectors.toList());
        System.out.println(tmp);


        System.out.println("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXX  stream test sorted on date and partion in 2 groups XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");

        Map<Boolean, List<HistoricFigure>> before = dataList.stream().sorted(Comparator.comparing(HistoricFigure::getBirthdate)).collect(Collectors.partitioningBy(h -> h.getBirthdate().isBefore(LocalDate.of(2000,1,1)) ));


        System.out.println("before 2000:");
        before.get(true).forEach(System.out::println);
        System.out.println("after 2000:");
        before.get(false).forEach(System.out::println);


        System.out.println("\nXXXXXXXXXXXXXXXXXXXXXXXXXXXX  stream test grouped by country XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
        Map<String,List<HistoricFigure>> tmpMap2 = dataList.stream().collect(Collectors.groupingBy(HistoricFigure::getCountry));

        System.out.println("\n\n");
        Map <String, List<HistoricFigure>> figureMap = dataList
                .stream()
                .sorted(Comparator.comparing(HistoricFigure::getName))
                .collect(Collectors.groupingBy(HistoricFigure::getCountry));
        SortedMap<String, List<HistoricFigure>> sortedFigureMap= new TreeMap<>(figureMap);
        sortedFigureMap.forEach((k, v) -> System.out.printf("%-8s %s\n",
                k, v
                        .stream()
                        .map(HistoricFigure::getName)
                        .collect(Collectors.joining(", "))));

        tmpMap2.forEach((country,list)-> System.out.printf("%s: %s\n",country,list.stream().map(HistoricFigure::getName).collect(Collectors.joining(","))));

    }
}
