package be.kdg.java2.model;

import java.util.*;
import java.util.function.Function;

public class HistoricFigures {

    private static class BirthComparator implements Comparator<HistoricFigure> {




        @Override
        public int compare(HistoricFigure o1, HistoricFigure o2) {
            return o1.getBirthdate().compareTo(o2.getBirthdate());
        }
    }



    private static class HeightComparator implements Comparator<HistoricFigure> {


        @Override
        public int compare(HistoricFigure o1, HistoricFigure o2) {
            return Double.compare(o1.getHeight(),o2.getHeight());
        }


    }

    private static class KillsComparator implements Comparator<HistoricFigure> {


        @Override
        public int compare(HistoricFigure o1, HistoricFigure o2) {
            return Integer.compare(o1.getKills(), o2.getKills());
        }
    }


    Set<HistoricFigure> setOfFigures= new TreeSet<>();


    public boolean add(HistoricFigure figure){
        return setOfFigures.add(figure);
    }


    public boolean remove(String name) throws IllegalAccessException {

        return setOfFigures.removeIf(t -> t.getName().equals(name));

    }


    public HistoricFigure search(String name) throws IllegalAccessException {



        return setOfFigures.stream().filter(t -> t.getName().equals(name)).findFirst().orElse(null);


    }



    public List<HistoricFigure> sortedBy(Function<HistoricFigure, Comparable> function){
        List<HistoricFigure> temp= new ArrayList<>(setOfFigures);

        temp.sort(Comparator.comparing(function));

        return temp;

    }




    public int getSize(){
        return setOfFigures.size();
    }


}
