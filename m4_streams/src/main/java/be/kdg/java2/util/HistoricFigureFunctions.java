package be.kdg.java2.util;/*
 * Wail Azoukane
 *15/10/2022
 *
 */

import be.kdg.java2.model.HistoricFigure;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

public class HistoricFigureFunctions {


    public static <T> List<T> filteredList(List<T> historicFigureslist, Predicate<T> predicate){

        return historicFigureslist.stream().filter(predicate).collect(Collectors.toList());

    }


    public static <T> Double average (List<T> historicFiguresList, ToDoubleFunction<T> mapper){

       return  historicFiguresList.stream().mapToDouble(mapper).average().orElse(Double.NaN);

    }


    public static <T> long countIf(List<T> historicFiguresList, Predicate<T> predicate){

       return historicFiguresList.stream().filter(predicate).count();

    }
}
