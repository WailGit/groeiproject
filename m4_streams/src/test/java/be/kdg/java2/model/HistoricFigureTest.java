package be.kdg.java2.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

/*
 * Wail Azoukane
 *19/10/2022
 *
 */class HistoricFigureTest {

     private HistoricFigure f1;
     private HistoricFigure f2;

    @BeforeEach
    void setUp() throws IllegalAccessException {
        f1= new HistoricFigure("Wail Azoukane","Morocco",1.94,2,GENDER.Male, LocalDate.of(2002,3,24));
        f2= new HistoricFigure("Majda Mekdade","Morocco",1.65,2,GENDER.Female, LocalDate.of(1973,5,16));
    }


    @Test
    void testEquals() {
        assertNotEquals(f1,f2,"The objects must be different from eachother ");

        HistoricFigure tmp = new HistoricFigure("Wail Azoukane","Belgium",1.94,16,GENDER.Male,LocalDate.of(2002,3,24));

        assertEquals(f1,tmp,String.format("De naam van beide objecten zouden hetzelfde moeten zijn\nTestObject: %sJouw object: %s",f1,tmp));
    }


    @Test
    void testIllegalHeight(){
        assertThrows(IllegalArgumentException.class,()-> f1.setHeight(-2),"There is not an exception throwed when initializing an argument that should be illegal");
    }

    @Test
    void testLegalHeight(){
        assertDoesNotThrow(()-> f1.setHeight(1.5),"There is an exception beeing throwed when it shouldn't!");
    }


    @Test
    void testCompareTo() throws IllegalAccessException {
        HistoricFigure tmp= new HistoricFigure();
        tmp.setName("Wail Azoukane");

        assertTrue(f1.compareTo(f2)>0,"The object should be different from each other!");
        assertEquals(0,f1.compareTo(tmp),"The object should be equal!");

        assertAll("Check if the figures are sorted", () -> assertEquals(f1.compareTo(f2), 10), () -> assertEquals(f1.compareTo(tmp), 0));



    }

    @Test
    void testHeight(){
        assertEquals(1.98,f1.getHeight(),0.05,"Height must be the same of a difference of 0.05");
    }
}