package be.kdg.java2.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/*
 * Wail Azoukane
 *19/10/2022
 *
 */class HistoricFiguresTest {
    private HistoricFigure f1;
    private HistoricFigure f2;
    private HistoricFigure f3;
    private HistoricFigure f4;
    private HistoricFigure f5;
    private HistoricFigures figures;



    @BeforeEach
    void setUp() throws IllegalAccessException {
        f1 = new HistoricFigure("Wail Azoukane", "Belgium", 1.94, 1, GENDER.Male, LocalDate.of(2002, 3, 24));
        f2 = new HistoricFigure("Sara Azoukane", "Morocco", 1.64, 2, GENDER.Female, LocalDate.of(2008, 3, 15));
        f3 = new HistoricFigure("Majda Mekdade", "Nigeria", 1.54, 3, GENDER.Female, LocalDate.of(1978, 5, 16));
        f4 = new HistoricFigure("Bilal Hafa", "Tamazigh", 1.74, 4, GENDER.Male, LocalDate.of(1980, 3, 24));
        f5 = new HistoricFigure("Danny Bikhan", "Russia", 1.44, 5, GENDER.Male, LocalDate.of(1800, 3, 24));
        figures = new HistoricFigures();
        figures.add(f1);
        figures.add(f2);
        figures.add(f3);
        figures.add(f4);
        figures.add(f5);
    }



    @Test
    void addTest() throws IllegalAccessException {

        HistoricFigure testFigure = new HistoricFigure("Conqueror", "Arabia", 1.85,2, GENDER.Male, LocalDate.of(2008,4,12));
        assertFalse(figures.add(f1));
        assertTrue(figures.add(testFigure));



    }

    @Test
    void removeTest() throws IllegalAccessException {
        assertEquals(5, figures.getSize(), "The size is not the same as it should be, before removing or adding!");
        figures.remove("Bilal Hafa");
        assertEquals(4, figures.getSize(), "There hasn't been anything removed");
        figures.remove("Unkown");
        assertEquals(4, figures.getSize(), "There has been something removed that doesn't exist in the collection");
    }


    @Test
    void sortTest() {
        List<HistoricFigure> sortedFigures = figures.sortedBy(HistoricFigure::getHeight);
        assertAll("Check if the figures are sorted", () -> assertEquals(sortedFigures.get(0).compareTo(f5), 0), () -> assertEquals(sortedFigures.get(1).compareTo(f3), 0), () -> assertEquals(sortedFigures.get(2).compareTo(f2), 0));

    }

    @Test
    void sortArrayTest() {

        List<HistoricFigure> sortedFigures = figures.sortedBy(HistoricFigure::getKills);
        List<HistoricFigure> sortedForSure = new ArrayList<>();

        sortedForSure.add(0, f1);
        sortedForSure.add(1, f2);
        sortedForSure.add(2, f3);
        sortedForSure.add(3, f4);
        sortedForSure.add(4, f5);
        assertArrayEquals(sortedForSure.toArray(), sortedFigures.toArray(), "The array is not sorted on kills!");


    }

}