import be.kdg.java2.data.Data;
import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigures;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.LogManager;

public class Demo_5 {
    public static void main(String[] args) throws Exception {

     HistoricFigure bad1= new HistoricFigure("Wail azoukane","",1.94,2,GENDER.Male,LocalDate.of(2002,3,24));
     HistoricFigure bad2= new HistoricFigure("sara azoukane","belgium",-4,0,GENDER.Female,LocalDate.of(2002,3,24));
     HistoricFigure bad3= new HistoricFigure("Sabrine azoukane","morocco",1.55,-3,GENDER.Female,LocalDate.of(2002,3,24));
     HistoricFigure bad4= new HistoricFigure("Majda Mekdade","belgium",1.64,2,null,LocalDate.of(2023,3,24));


     InputStream inputStream = Demo_5.class.getResourceAsStream("./logging.properties");
     LogManager.getLogManager().readConfiguration(inputStream);


        HistoricFigures test = new HistoricFigures();
        test.add(bad1);
        test.add(bad2);
        test.add(bad3);
        test.add(bad4);

        test.remove("Wail azoukane");
        test.remove("sara azoukane");
        test.remove("Sabrine azoukane");
        test.remove("Majda Mekdade");



    }
}