package be.kdg.java2.logging;/*
 * Wail Azoukane
 *21/10/2022
 *
 */

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class SmallLogFormatter extends Formatter {
    @Override
    public String format(LogRecord record) {

        LocalDateTime lt= LocalDateTime.ofInstant(record.getInstant(), ZoneId.systemDefault());

        return String.format("%s Level: %s melding: '%s'",lt,record.getLevel(),MessageFormat.format(record.getMessage(), record.getParameters()));

    }
}
