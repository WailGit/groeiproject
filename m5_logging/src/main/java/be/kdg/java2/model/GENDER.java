package be.kdg.java2.model;

public enum GENDER {
    Male, Female, UNKOWN;

    @Override
    public String toString() {

        return switch (this) {
            case Male -> "Male";
            case Female -> "Female";
            default -> "Unkown";
        };
    }
}
