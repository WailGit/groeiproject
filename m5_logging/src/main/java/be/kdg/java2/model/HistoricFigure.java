package be.kdg.java2.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.logging.Level;
import java.util.logging.Logger;

import static be.kdg.java2.model.GENDER.UNKOWN;


/*
* DummyWaarde voor datum 1/1/1975
* DummyWaarde voor lengte: 1
* */

public class HistoricFigure implements Comparable<HistoricFigure> {

    private Logger logger=Logger.getLogger("be.kdg.java2.model.HistoricFigure");
    private String name;
    private String country;
    private double height;
    private int kills;
    private GENDER gender;
    private LocalDate birthdate;

    public HistoricFigure() throws IllegalAccessException {
        this("Unkown","Unkown",1,0,UNKOWN,LocalDate.of(1975,1,1));
    }



    public HistoricFigure(String name, String country, double height, int kills, GENDER gender, LocalDate birthdate) throws IllegalAccessException {

        try{
            setName(name);
            setCountry(country);
            setBirthdate(birthdate);
            setHeight(height);
            setKills(kills);
            setGender(gender);
        }catch(Exception e){
            System.out.print(e.getMessage());
        }

    }



    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public double getHeight() {
        return height;
    }

    public int getKills() {
        return kills;
    }

    public GENDER getGender() {
        return gender;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setName(String name) throws IllegalAccessException {
        if(name== null || name.equals("")){
           // throw new IllegalArgumentException("Please enter a valid name in!");
            logger.log(Level.SEVERE,String.format("This is %s not a valid name for %s as an object",name,name));
        }

        this.name=name;
    }

    public void setCountry(String country) {
        if(country== null || country.equals("")){
            //throw new IllegalArgumentException("Please enter a valid coutryname in!");
            logger.log(Level.SEVERE,String.format("%s is not a valid countryname for %s",country,name));
        }

        this.country = country;
    }

    public void setHeight(double height) {
        if( height<=0){
            //throw new IllegalArgumentException("Height can not be negative or 0!");

            logger.log(Level.SEVERE,String.format("Height can not be %.2f for %s",height,name));
        }
        this.height = height;
    }

    public void setKills(int kills) {
        if( kills<0){
           // throw new IllegalArgumentException("Amount of kills can not be negative!");
            logger.log(Level.SEVERE,String.format("%d is not a valid amount of kills for %s",kills,name));
        }
        this.kills = kills;
    }

    public void setGender(GENDER gender) {
        if(gender== null){
           // throw new IllegalArgumentException("Please choose a gender M/F!");
            logger.log(Level.SEVERE,String.format("%s is not a valid gender for %s",gender,name));
        }
        this.gender = gender;
    }

    public void setBirthdate(LocalDate birthdate) {
        if (birthdate==null || birthdate.getYear()>LocalDate.now().getYear() ){
            //throw new IllegalArgumentException("Please enter a birthdate that is less then the currentyear");
            logger.log(Level.SEVERE,String.format("%s is not a valid birthdate for %s",birthdate,name));
        }
        this.birthdate = birthdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoricFigure figure = (HistoricFigure) o;

        return name.equals(figure.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public int compareTo(HistoricFigure o) {
        return this.name.compareTo(o.getName());
    }


    @Override
    public String toString() {

        DateTimeFormatter format= DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);

        return String.format("name: %s country:%s height:%.2f kills:%d gender:%s birthdate:%s\n",name,country,height,kills,gender.toString(),format.format(birthdate));

    }
}
