package be.kdg.java2.model;

import java.io.Serial;
import java.io.Serializable;
import java.util.*;

public class HistoricFigures implements Serializable {
    @Serial
    private static final long serialVersionUID= 1L;
    private static class BirthComparator implements Comparator<HistoricFigure> {


        @Override
        public int compare(HistoricFigure o1, HistoricFigure o2) {
            return o1.getBirthdate().compareTo(o2.getBirthdate());
        }
    }

    private static class HeightComparator implements Comparator<HistoricFigure> {


        @Override
        public int compare(HistoricFigure o1, HistoricFigure o2) {
            return Double.compare(o1.getHeight(),o2.getHeight());
        }
    }

    private static class KillsComparator implements Comparator<HistoricFigure> {


        @Override
        public int compare(HistoricFigure o1, HistoricFigure o2) {
            return Integer.compare(o1.getKills(), o2.getKills());
        }
    }


    Set<HistoricFigure> setOfFigures= new TreeSet<>();


    public boolean add(HistoricFigure figure){
        return setOfFigures.add(figure);
    }


    public boolean remove(String name) throws IllegalAccessException {
        HistoricFigure temp= new HistoricFigure();
        temp.setName(name);

        return setOfFigures.remove(temp);
    }


    public HistoricFigure search(String name) throws IllegalAccessException {

        HistoricFigure temp= new HistoricFigure();
        temp.setName(name);
        // dit kon ook met de contains functie, maar dan kon je niet dat object returnen!
        for(HistoricFigure figure: setOfFigures){
            if(temp.equals(figure)){
                return figure;
            }
        }
        return null;
    }


    public List<HistoricFigure> sortedOnHeight(){
        List<HistoricFigure> temp= new ArrayList<>(setOfFigures);
        temp.sort(new HeightComparator());

        return temp;
    }
    public List<HistoricFigure> sortedOnBirth(){
        List<HistoricFigure> temp= new ArrayList<>(setOfFigures);
        temp.sort(new BirthComparator());

        return temp;
    }
    public List<HistoricFigure> sortedOnKills(){
        List<HistoricFigure> temp= new ArrayList<>(setOfFigures);
        temp.sort(new KillsComparator());

        return temp;
    }


    public int getSize(){
        return setOfFigures.size();
    }



    @Override
    public boolean equals(Object o) {
        boolean allmatch = true;
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HistoricFigures figure = (HistoricFigures) o;
        for(HistoricFigure fig: setOfFigures){
            boolean current= false;
            for(HistoricFigure f:figure.setOfFigures){
                if ( f.equals(fig)){
                   current=true;
                   break;
                }
            }

            if ( !current){
                allmatch=false;
                break;
            }
        }

        return allmatch;



    }

    @Override
    public int hashCode() {

        return setOfFigures.hashCode();
    }


}
