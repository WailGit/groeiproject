package be.kdg.java2.persist;/*
 * Wail Azoukane
 *12/11/2022
 *
 */

import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigures;

import java.io.*;

public class HistoricFigureSerializer {

    private final String FILENAME; // db/dictators.ser

    public HistoricFigureSerializer(String filename) {
        FILENAME = filename;
    }


    public void serialize(HistoricFigures figures) throws IOException {

        try{
            ObjectOutputStream output = new ObjectOutputStream( new FileOutputStream(FILENAME));
            output.writeObject(figures);
        }
        catch(IOException e){
            throw e;
        }


    }


    public HistoricFigures deserialize() throws IOException, ClassNotFoundException{
        try{
            ObjectInputStream data2 = new ObjectInputStream(new FileInputStream(FILENAME));
            return (HistoricFigures) data2.readObject();

        }catch(IOException e){
            throw e;
        }
        catch(ClassNotFoundException c){
            throw new ClassNotFoundException("dummy");
        }

    }

}
