package be.kdg.historicfigureproject.persist;/*
 * Wail Azoukane
 *12/11/2022
 *
 */

import be.kdg.java2.data.Data;
import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigureDbDao;
import be.kdg.java2.model.HistoricFigures;
import org.junit.jupiter.api.*;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class HistoricFigureDbDaoTest {

    private HistoricFigure f1;


    private static HistoricFigureDbDao dao;

    @BeforeAll
    static void init() throws SQLException {
        dao= new HistoricFigureDbDao( "jdbc:hsqldb:file:db/mydata");

    }

    @AfterAll
    static void doneTesting() throws SQLException {
        dao.close();
    }

    @AfterEach
    void deleteEverything() throws IllegalAccessException, SQLException {
        dao.delete("*");
    }

    @BeforeEach
    void setUp() throws IllegalAccessException, SQLException {
        dao.createTable();
        for(HistoricFigure figure : Data.getData()){
            dao.insert(figure);
        }
    }



    @Test
    void testInsert() throws IllegalAccessException, SQLException {
        assertEquals(15,Data.getData().size());
        List<HistoricFigure> tmp= dao.sortedOnHeight();
        assertEquals(15,tmp.size());
        f1 = new HistoricFigure("Wail Azoukane", "Belgium", 1.94, 1, GENDER.Male, LocalDate.of(2002, 3, 24));
        dao.insert(f1);
        tmp=dao.sortedOnHeight();
        assertEquals(16,tmp.size());


    }


    @Test

    public void testRetrieve(){
        assertNotNull(dao.retrieve("Jack The Ripper"), "Failed to retrieve data");
    }
    @Test
    void testRetrieveUpdate() throws  IOException, ClassNotFoundException {
        HistoricFigure figure = dao.retrieve("Jack The Ripper");
        figure.setKills(2);
        dao.update(figure);
        assertEquals(figure.getKills(),dao.retrieve("Jack The Ripper").getKills());
    }

    @Test
    void testDelete(){
        List<HistoricFigure> tmp= dao.sortedOnHeight();
        assertEquals(15,tmp.size());
        dao.delete("Jack The Ripper");
        tmp=dao.sortedOnHeight();
        assertEquals(14,tmp.size());
    }


    @Test
    void testSort() throws IllegalAccessException {
       HistoricFigure  f1 = new HistoricFigure("Jack The Ripper", "Belgium", 1, 1, GENDER.Male, LocalDate.of(2002, 3, 24));
        HistoricFigure f2 = new HistoricFigure("Elizabeth 2", "Morocco", 1.62, 2, GENDER.Female, LocalDate.of(2008, 3, 15));
        HistoricFigure f3 = new HistoricFigure("Napoleon", "Nigeria", 1.68, 3, GENDER.Female, LocalDate.of(1978, 5, 16));
        HistoricFigure f4 = new HistoricFigure("Isaac Newton", "Tamazigh", 1.68, 4, GENDER.Male, LocalDate.of(1980, 3, 24));
        HistoricFigures figures = new HistoricFigures();
        figures.add(f1);
        figures.add(f2);
        figures.add(f3);
        figures.add(f4);
        List<HistoricFigure> tmp= dao.sortedOnHeight();
        assertAll(
                () -> assertEquals(f1, tmp.get(0), "List is not sorted!"),
                () -> assertEquals(f2, tmp.get(1), "List is not sorted!"),
                () -> assertEquals(f3, tmp.get(2), "List is not sorted!"));


    }




}
