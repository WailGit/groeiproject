package be.kdg.historicfigureproject.persist;

import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigures;
import be.kdg.java2.persist.HistoricFigureSerializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class HistoricFigureSerializerTest {
    private HistoricFigure f1;
    private HistoricFigure f2;
    private HistoricFigure f3;
    private HistoricFigure f4;
    private HistoricFigure f5;
    private HistoricFigures figures;
    private HistoricFigureSerializer serializer;


    @BeforeEach
    void setUp() throws IllegalAccessException {
        figures = new HistoricFigures();
        serializer= new HistoricFigureSerializer("db/figures.txt");
        f1 = new HistoricFigure("Wail Azoukane", "Belgium", 1.94, 1, GENDER.Male, LocalDate.of(2002, 3, 24));
        f2 = new HistoricFigure("Sara Azoukane", "Morocco", 1.64, 2, GENDER.Female, LocalDate.of(2008, 3, 15));
        f3 = new HistoricFigure("Majda Mekdade", "Nigeria", 1.54, 3, GENDER.Female, LocalDate.of(1978, 5, 16));
        f4 = new HistoricFigure("Bilal Hafa", "Tamazigh", 1.74, 4, GENDER.Male, LocalDate.of(1980, 3, 24));
        f5 = new HistoricFigure("Danny Bikhan", "Russia", 1.44, 5, GENDER.Male, LocalDate.of(1800, 3, 24));
        figures.add(f1);
        figures.add(f2);
        figures.add(f3);
        figures.add(f4);
        figures.add(f5);
    }


    @Test
    void testSerializer() throws IllegalAccessException {

        assertDoesNotThrow( () -> serializer.serialize(figures), "Dit hoort  exception te geven");

    }

    @Test
    void testDeserializer() throws  IOException, ClassNotFoundException {
        serializer.serialize(figures);
        HistoricFigures tmp = serializer.deserialize();
        assertEquals(figures,tmp);
    }



}
