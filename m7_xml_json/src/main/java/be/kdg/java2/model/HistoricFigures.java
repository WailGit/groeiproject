package be.kdg.java2.model;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.*;

@XmlRootElement(name= "historicfigures")
public class HistoricFigures {


    private static class BirthComparator implements Comparator<HistoricFigure> {


        @Override
        public int compare(HistoricFigure o1, HistoricFigure o2) {
            return o1.getBirthdate().compareTo(o2.getBirthdate());
        }
    }

    private static class HeightComparator implements Comparator<HistoricFigure> {


        @Override
        public int compare(HistoricFigure o1, HistoricFigure o2) {
            return Double.compare(o1.getHeight(),o2.getHeight());
        }
    }

    private static class KillsComparator implements Comparator<HistoricFigure> {


        @Override
        public int compare(HistoricFigure o1, HistoricFigure o2) {
            return Integer.compare(o1.getKills(), o2.getKills());
        }
    }


    List<HistoricFigure> setOfFigures= new ArrayList<>();


    public boolean add(HistoricFigure figure){
        return setOfFigures.add(figure);
    }


    public boolean remove(String name) throws IllegalAccessException {
        HistoricFigure temp= new HistoricFigure();
        temp.setName(name);

        return setOfFigures.remove(temp);
    }


    public HistoricFigure search(String name) throws IllegalAccessException {

        HistoricFigure temp= new HistoricFigure();
        temp.setName(name);
        // dit kon ook met de contains functie, maar dan kon je niet dat object returnen!
        for(HistoricFigure figure: setOfFigures){
            if(temp.equals(figure)){
                return figure;
            }
        }
        return null;
    }


    public List<HistoricFigure> sortedOnHeight(){
        List<HistoricFigure> temp= new ArrayList<>(setOfFigures);
        temp.sort(new HeightComparator());

        return temp;
    }
    public List<HistoricFigure> sortedOnBirth(){
        List<HistoricFigure> temp= new ArrayList<>(setOfFigures);
        temp.sort(new BirthComparator());

        return temp;
    }
    public List<HistoricFigure> sortedOnKills(){
        List<HistoricFigure> temp= new ArrayList<>(setOfFigures);
        temp.sort(new KillsComparator());

        return temp;
    }


    public int getSize(){
        return setOfFigures.size();
    }

    public List<HistoricFigure> getSetOfFigures() {
        return setOfFigures;
    }

    @XmlElement(name = "historicfigure")
    public void setSetOfFigures(List<HistoricFigure> setOfFigures) {
        this.setOfFigures = setOfFigures;
    }
}
