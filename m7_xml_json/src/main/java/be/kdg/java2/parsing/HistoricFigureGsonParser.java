package be.kdg.java2.parsing;/*
 * Wail Azoukane
 *24/11/2022
 *
 */
import be.kdg.java2.parsing.LocalDateGsonAdapter;
import be.kdg.java2.model.HistoricFigures;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.time.LocalDate;

public class HistoricFigureGsonParser {


    public static void writeJson(HistoricFigures figures, String fileName) throws IOException {

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalDate.class, new LocalDateGsonAdapter().nullSafe());
        Gson gson = builder.setPrettyPrinting().create();


        String jsonString = gson.toJson(figures);


        try (
                FileWriter jsonWriter = new FileWriter(fileName)) {
            jsonWriter.write(jsonString);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public static HistoricFigures readJson(String fileName) throws FileNotFoundException {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalDate.class, new LocalDateGsonAdapter().nullSafe());
        Gson gson = builder.setPrettyPrinting().create();


        BufferedReader data = new BufferedReader(new FileReader(fileName));

        return gson.fromJson(data,HistoricFigures.class);
    }
}
