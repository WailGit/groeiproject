package be.kdg.java2.parsing;/*
 * Wail Azoukane
 *24/11/2022
 *
 */

import java.io.File;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

import java.io.File;


public class HistoricFigureJaxbParser {

    public static void JaxbWriteXml(String file, Object root) throws JAXBException {

        JAXBContext context = JAXBContext.newInstance(root.getClass());
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(root, new File(file));


    }



    public static <T> T JaxbReadXml(String file, Class<T> typeParameterClass) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(typeParameterClass);
        Unmarshaller u = jc.createUnmarshaller();
        File f = new File(file);
        T multiClass = (T) u.unmarshal(f);

        return multiClass;

    }
}
