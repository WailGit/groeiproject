package be.kdg.java2.parsing;/*
 * Wail Azoukane
 *24/11/2022
 *
 */

import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigures;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;



public class HistoricFigureStaxParser {

    private HistoricFigures figures;
    private XMLStreamWriter writer;


    public HistoricFigureStaxParser(HistoricFigures figures, String path) throws IOException, XMLStreamException {

        try {
            this.figures = figures;
            FileWriter file = new FileWriter(path, StandardCharsets.UTF_8);
            this.writer = XMLOutputFactory.newInstance().createXMLStreamWriter(file);
            writer = new IndentingXMLStreamWriter(writer);
        }
        catch (IOException e){
            throw e;
        }
    }





    public void staxWriteXML() {
        try{ writer.writeStartDocument();
            writer.writeStartElement("historicfigures");
            figures.sortedOnHeight().forEach(this::writeElement);
            writer.writeEndElement();
            writer.writeEndDocument();
            writer.close();
        }catch(XMLStreamException e){
            e.printStackTrace();
        }

    }

    private void writeElement(HistoricFigure figure)  {

        try{ writer.writeStartElement("historicfigure");
            writer.writeAttribute("gender",figure.getGender().name());

            writer.writeStartElement("name");
            writer.writeCharacters(figure.getName());
            writer.writeEndElement();


            writer.writeStartElement("country");
            writer.writeCharacters(figure.getCountry());
            writer.writeEndElement();

            writer.writeStartElement("height");
            writer.writeCharacters(String.valueOf(figure.getHeight()));
            writer.writeEndElement();

            writer.writeStartElement("kills");
            writer.writeCharacters(String.valueOf(figure.getKills()));
            writer.writeEndElement();


            writer.writeStartElement("birth");
            writer.writeCharacters(figure.getBirthdate().toString());
            writer.writeEndElement();


            writer.writeEndElement();}
        catch(XMLStreamException e){
            e.printStackTrace();
        }



    }

}
