package be.kdg.java2.parsing;/*
 * Wail Azoukane
 *24/11/2022
 *
 */


import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigures;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;


public class HistoricFiguresDomParser {


    public static HistoricFigures domReadXML(String fileName) throws ParserConfigurationException {
        HistoricFigures figures = new HistoricFigures();

        try {


            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(fileName));

            Element rootElement = doc.getDocumentElement();
            NodeList figureNodes = rootElement.getChildNodes();

            for( int i=0;i<figureNodes.getLength();i++){
                if( figureNodes.item(i).getNodeType() != Node.ELEMENT_NODE){
                    continue;
                }


                Element e = (Element) figureNodes.item(i);
                String str= e.getAttribute("gender");
                Element name =(Element) e.getElementsByTagName("name").item(0);

                Element birt =(Element) e.getElementsByTagName("birth").item(0);
                Element country =(Element) e.getElementsByTagName("country").item(0);
                Element height =(Element) e.getElementsByTagName("height").item(0);
                Element kills =(Element) e.getElementsByTagName("kills").item(0);

                figures.add(
                        makeHistoricFigureObject(name.getTextContent(),country.getTextContent(),Double.parseDouble(height.getTextContent()),Integer.parseInt(kills.getTextContent()),GENDER.valueOf(e.getAttribute("gender")),LocalDate.parse(birt.getTextContent())
                        )
                );
            }





        } catch (SAXException | IOException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }


        return figures;



    }

    private static HistoricFigure makeHistoricFigureObject(String name, String country, double height, int kills, GENDER gender, LocalDate birthdate) throws IllegalAccessException {
        return new HistoricFigure( name,  country,  height,  kills,  gender,  birthdate);
    }


}
