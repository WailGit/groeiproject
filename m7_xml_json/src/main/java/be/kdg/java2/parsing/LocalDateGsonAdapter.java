package be.kdg.java2.parsing;/*
 * Wail Azoukane
 *24/11/2022
 *
 */

import com.google.gson.TypeAdapter;
import com.google.gson.stream.*;
import java.io.IOException;
import java.time.LocalDate;


public class LocalDateGsonAdapter extends TypeAdapter<LocalDate> {

    @Override
    public void write(final JsonWriter jsonWriter,
                      final LocalDate localDate) throws IOException {
        jsonWriter.value(localDate.toString());
    }
    @Override
    public LocalDate read(final JsonReader jsonReader)
            throws IOException {
        return LocalDate.parse(jsonReader.nextString());
    }
}
