package be.kdg.java2.parsing;/*
 * Wail Azoukane
 *24/11/2022
 *
 */
import jakarta.xml.bind.annotation.adapters.XmlAdapter;

import java.time.LocalDate;

public class LocalDateXmlAdapter extends XmlAdapter<String, LocalDate>{

    public LocalDate unmarshal(String myString) throws Exception {
        return LocalDate.parse(myString);
    }
    public String marshal(LocalDate myDate) throws Exception {
        return myDate.toString();
    }
}
