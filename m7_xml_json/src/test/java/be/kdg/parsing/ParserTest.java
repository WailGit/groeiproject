package be.kdg.parsing;/*
 * Wail Azoukane
 *24/11/2022
 *
 */

import be.kdg.java2.data.Data;
import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigures;
import be.kdg.java2.parsing.HistoricFigureGsonParser;
import be.kdg.java2.parsing.HistoricFigureJaxbParser;
import be.kdg.java2.parsing.HistoricFigureStaxParser;
import be.kdg.java2.parsing.HistoricFiguresDomParser;
import jakarta.xml.bind.JAXBException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParserTest {

    private HistoricFigures figures;

    @BeforeEach
    public void setUp() throws IllegalAccessException {
        List<HistoricFigure> tmp= Data.getData();
        figures= new HistoricFigures();
        tmp.forEach(figure -> figures.add(figure));
    }


    @Test
    public void testStaxDom() throws XMLStreamException, IOException, ParserConfigurationException {
        HistoricFigureStaxParser stax= new HistoricFigureStaxParser(figures,"src/datafiles/staxHistoricFigures.xml");
        stax.staxWriteXML();
        HistoricFigures thruReader =HistoricFiguresDomParser.domReadXML("src/datafiles/staxHistoricFigures.xml");


        List<HistoricFigure> sortedFromInitialized= figures.sortedOnHeight();
        List<HistoricFigure> sortedFromReader= thruReader.sortedOnHeight();


        assertAll(
                () -> assertEquals(sortedFromInitialized.get(0), sortedFromReader.get(0), "De objects are not the same !"),
                () -> assertEquals(sortedFromInitialized.get(1), sortedFromReader.get(1), "De objects are not the same !"),
                () -> assertEquals(sortedFromInitialized.get(2), sortedFromReader.get(2), "De objects are not the same !"));
    }


    @Test
    public void testJaxb() throws XMLStreamException, IOException, ParserConfigurationException, JAXBException {
        HistoricFigureJaxbParser.JaxbWriteXml("src/datafiles/jaxbHistoricFigures.xml",figures);

        HistoricFigures thruReader =HistoricFigureJaxbParser.JaxbReadXml("src/datafiles/jaxbHistoricFigures.xml",figures.getClass());


        List<HistoricFigure> sortedFromInitialized= figures.sortedOnHeight();
        List<HistoricFigure> sortedFromReader= thruReader.sortedOnHeight();


        assertAll(
                () -> assertEquals(sortedFromInitialized.get(0), sortedFromReader.get(0), "De objects are not the same !"),
                () -> assertEquals(sortedFromInitialized.get(1), sortedFromReader.get(1), "De objects are not the same !"),
                () -> assertEquals(sortedFromInitialized.get(2), sortedFromReader.get(2), "De objects are not the same !"));
    }

    @Test
    public void testGson() throws XMLStreamException, IOException, ParserConfigurationException {

        HistoricFigureGsonParser.writeJson(figures,"src/datafiles/HistoricFigures.json");

        HistoricFigures thruReader =HistoricFigureGsonParser.readJson("src/datafiles/HistoricFigures.json");


        List<HistoricFigure> sortedFromInitialized= figures.sortedOnHeight();
        List<HistoricFigure> sortedFromReader= thruReader.sortedOnHeight();


        assertAll(
                () -> assertEquals(sortedFromInitialized.get(0), sortedFromReader.get(0), "De objects are not the same !"),
                () -> assertEquals(sortedFromInitialized.get(1), sortedFromReader.get(1), "De objects are not the same !"),
                () -> assertEquals(sortedFromInitialized.get(2), sortedFromReader.get(2), "De objects are not the same !"));
    }


}
