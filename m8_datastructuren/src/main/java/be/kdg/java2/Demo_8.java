package be.kdg.java2;/*
 * Wail Azoukane
 *2/12/2022
 *
 */

import be.kdg.java2.kollections.Kollections;
import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigureFactory;
import be.kdg.java2.kollections.lists.List;

import java.time.LocalDate;
import java.util.stream.Stream;


public class Demo_8 {
    public static void main(String[] args) throws Exception {

        var empty = HistoricFigureFactory.newEmptyHistoricFigure();
        System.out.printf("Empty :\n%s\n\n", empty);

        var filed = HistoricFigureFactory.newFilledHistoricFigure("Lord Ozan","Japan",2.00,20,GENDER.Male,LocalDate.of(1980,6,22) );
        System.out.printf("Filled :\n%s\n\n", filed);

        System.out.println("30 random figures sorted :\n");
        Stream.generate(() -> {
            try {
                return (Object) HistoricFigureFactory.newRandomHistoricFigure();
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }).limit(30).sorted().forEach(System.out::println);

        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Compare arrayList & linked list XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

        PerformanceTester.compareArrayListAndLinkedList(20000);

        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        List<HistoricFigure> tmp2=  PerformanceTester.randomList(30);

        Kollections.selectionSort(tmp2);
        for (int i = 0; i < tmp2.size(); i++){
            System.out.println(tmp2.get(i));
        }

        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXMERGESORTXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        List<HistoricFigure> tmp3=  PerformanceTester.randomList(30);

        Kollections.mergeSort(tmp3);
        for(int i=0;i<tmp3.size();i++){
            System.out.println(tmp3.get(i));
        }

        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXSELECTIONTESTXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

        PerformanceTester.testSelectionSort();
        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXMERGETESTXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

        PerformanceTester.testMergeSort();

        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXQuicksortXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

        List<HistoricFigure> tmp4 = PerformanceTester.randomList(30);

        Kollections.quickSort(tmp4);
        for (int i = 0; i < tmp4.size(); i++) {
            System.out.println(tmp4.get(i));
        }
        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXSearchXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

        List<HistoricFigure> tmp5 = PerformanceTester.randomList(10);
        HistoricFigure wail= new HistoricFigure("zzzzz","belgium",1.69,5, GENDER.Male, LocalDate.now());
        tmp5.add(wail);
        Kollections.selectionSort(tmp5);

        System.out.println(Kollections.binarySearch(tmp5,wail));
        System.out.println(Kollections.binarySearch(tmp5,new HistoricFigure()));


        System.out.println(Kollections.lineairSearch(tmp5,wail));
        System.out.println(Kollections.lineairSearch(tmp5,new HistoricFigure()));



        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXMAPPPPXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        PerformanceTester.compareListMapToHasMap(1000);
        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXSETTTXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        PerformanceTester.compareArraySetToTreeSet(1000);

    }
}
