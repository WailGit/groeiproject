package be.kdg.java2;/*
 * Wail Azoukane
 *2/12/2022
 *
 */

import be.kdg.java2.kollections.lists.ArrayList;
import be.kdg.java2.kollections.Kollections;
import be.kdg.java2.kollections.lists.LinkedList;
import be.kdg.java2.kollections.lists.List;
import be.kdg.java2.kollections.maps.HashMap;
import be.kdg.java2.kollections.maps.ListMap;
import be.kdg.java2.kollections.maps.Map;
import be.kdg.java2.kollections.sets.ArraySet;
import be.kdg.java2.kollections.sets.Set;
import be.kdg.java2.kollections.sets.TreeSet;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigureFactory;

import java.util.Random;

public class PerformanceTester {

    private static int suffix=0;

    public static List<HistoricFigure> randomList(int n) {
        List<HistoricFigure> myList = new LinkedList<>();
        new Random().ints(n).forEach(i -> {
            try {
                myList.add(HistoricFigureFactory.newRandomHistoricFigure());
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        });
        return myList;

    }

    public static void compareArrayListAndLinkedList(int n) {
        List<HistoricFigure> linked = new LinkedList<>();
        List<HistoricFigure> array = new ArrayList<>();

        List<Object> arrayList = new ArrayList<>();
        List<Object> linkedList = new LinkedList<>();
        // add at begining Arraylist
        long startTimeMillis = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> {
            try {
                arrayList.add(HistoricFigureFactory.newRandomHistoricFigure());
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        });
        long duration = System.currentTimeMillis() - startTimeMillis;
        System.out.printf("adding %d elements to Arraylist: %d ms.\n", n, duration);
        // add at beginningLinkedList
        startTimeMillis = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> {
            try {
                linkedList.add(HistoricFigureFactory.newRandomHistoricFigure());
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        });
        duration = System.currentTimeMillis() - startTimeMillis;
        System.out.printf("adding %d elements to LinkedList: %d ms.\n", n, duration);
        // get at end ArrayList
        startTimeMillis = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> arrayList.get(n-1));
        duration = System.currentTimeMillis() - startTimeMillis;
        System.out.printf("retrieving %d elements from ArrayList: %d ms.\n", n, duration);
        // get at end LinkedList
        startTimeMillis = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> linkedList.get(n-1));
        duration = System.currentTimeMillis() - startTimeMillis;
        System.out.printf("retrieving %d elements from LinkedList: %d ms.\n", n, duration);


    }




    public static void testSelectionSort() {


        for(int n=1000;n<20000;n+=1000){
            List<HistoricFigure> tmp=  PerformanceTester.randomList(n);
            Kollections.selectionSort(tmp);
            System.out.printf("for a list of %d: %d compares\n",n,HistoricFigure.compareCounter);

        }
    }

    public static void testMergeSort() {
        for(int n=1000;n<20000;n+=1000){
            List<HistoricFigure> tmp=  PerformanceTester.randomList(n);
            Kollections.mergeSort(tmp);
            System.out.printf("for a list of %d: %d compares\n",n,HistoricFigure.compareCounter);

        }
    }


    public static void compareListMapToHasMap(int sizeMap) throws IllegalAccessException {

        ListMap<HistoricFigure, String> listMap = new ListMap<>();
        HashMap<HistoricFigure,String> hashMap = new HashMap<>();



        fillMap(listMap,sizeMap);
        HistoricFigure.equalsCounter=0;
        suffix=0;
        long startTime = System.nanoTime();
        for(int i =0;i<listMap.size();i++){
            HistoricFigure figure= new HistoricFigure();
            figure.setName(String.format("Unkown%d",suffix++));
            listMap.get(figure);
        }
        long duration = System.nanoTime() -startTime;
        System.out.println(String.format("Listmap: n = %d, equalscount= %d, nanosex= %d ",sizeMap,HistoricFigure.equalsCounter,duration));
        suffix=0;
        HistoricFigure.equalsCounter=0;





        fillMap(hashMap,sizeMap);
        suffix=0;
        startTime = System.nanoTime();
        for(int i =0;i<hashMap.size();i++){
            HistoricFigure figure= new HistoricFigure();
            figure.setName(String.format("Unkown%d",suffix++));
            hashMap.get(figure);
        }
        duration = System.nanoTime() -startTime;
        System.out.println(String.format("Hashmap: n = %d, equalscount= %d, nanosex= %d ",sizeMap,HistoricFigure.equalsCounter,duration));
        HistoricFigure.equalsCounter=0;

    }


    public static void compareArraySetToTreeSet(int sizeSet) throws IllegalAccessException {
        ArraySet<HistoricFigure> arraySet=new ArraySet<>();
        TreeSet<HistoricFigure> tree= new TreeSet<>();

        HistoricFigure.equalsCounter=0;
        suffix=0;
        long startTime = System.nanoTime();
       fillSet( arraySet,sizeSet);
        long duration = System.nanoTime() -startTime;
        System.out.println(String.format("ArraySetmap: n = %d, equalscount= %d",sizeSet,HistoricFigure.equalsCounter));
        System.out.println(String.format("ArraySetmap: n = %d, comparecount= %d",sizeSet,HistoricFigure.compareCounter));
        System.out.println(String.format("ArraySetmap: n = %d, nanosex= %d ",sizeSet,duration));





        HistoricFigure.equalsCounter=0;
        suffix=0;
        startTime = System.nanoTime();
        fillSet(tree,sizeSet);
        duration = System.nanoTime() -startTime;
        System.out.println(String.format("TreeSet: n = %d, equalscount= %d",sizeSet,HistoricFigure.equalsCounter));
        System.out.println(String.format("TreeSet: n = %d, comparecount= %d",sizeSet,HistoricFigure.compareCounter));
        System.out.println(String.format("TreeSet: n = %d, nanosex= %d ",sizeSet,duration));
        suffix=0;
        HistoricFigure.equalsCounter=0;
    }


    private static void fillSet(Set<HistoricFigure> set, int n){
        new Random().ints(n).forEach(i -> {
            try {
                HistoricFigure figure= HistoricFigureFactory.newEmptyHistoricFigure();
                figure.setName(String.format("%s%d",figure.getName(),suffix++));
                set.add(figure);

            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        });
    }


    private static void fillMap(Map<HistoricFigure,String> map,int n){

        new Random().ints(n).forEach(i -> {
            try {
                HistoricFigure figure= HistoricFigureFactory.newEmptyHistoricFigure();
                figure.setName(String.format("%s%d",figure.getName(),suffix++));
                map.put(figure,String.format("Ik ben de waarde %s",figure.getName()));

            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        });

    }
}
