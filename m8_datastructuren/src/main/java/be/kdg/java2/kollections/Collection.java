package be.kdg.java2.kollections;/*
 * Wail Azoukane
 *10/12/2022
 *
 */

public interface Collection<E> {
    void add(E element);
    boolean remove(E element);
    boolean contains(E element);
    int size();
}

