package be.kdg.java2.kollections.lists;/*
 * Wail Azoukane
 *2/12/2022
 *
 */


import be.kdg.java2.kollections.Kollections;

public class LinkedList<E> implements List<E> {
    static class Node<V> {
        V value;
        Node<V> next;

        public Node(V value) {
            this.value = value;
        }
    }

    private Node<E> root;
    private int size;

    public LinkedList() {
    }

    @Override
    public void add(int index, E element) {
        if (index < 0 || index > size) throw new IndexOutOfBoundsException();
        if (index == 0) {
            Node<E> newNode = new Node<>(element);
            newNode.next = root;
            root = newNode;
        } else {
            Node<E> node = root;
            for (int i = 0; i < index - 1; i++) {
                node = node.next;
            }
            Node<E> newNode = new Node<>(element);
            newNode.next = node.next;
            node.next = newNode;
        }
        size++;
    }

    @Override
    public void add(E element) {
        add(size, element);
    }



    @Override
    public void set(int index, E element) {
        if (index > this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        Node<E> node = root;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        node.value = element;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public E remove(int index) {
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        if (index == 0) {
            E oldElement = root.value;
            root = root.next;
            size--;
            return oldElement;
        } else {
            Node<E> beforeNode = root;
            for (int i = 1; i < index; i++) {
                beforeNode = beforeNode.next;
            }
            E oldElement = beforeNode.next.value;
            beforeNode.next = beforeNode.next.next;
            size--;
            return oldElement;
        }
    }

    @Override
    public E get(int index) {
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        if (root == null){
            return null;
        }
        Node<E> node = root;
        for (int i = 0; i < index; i++){
            node = node.next;
        }
        return node.value;
    }

    @Override
    public int indexOf(E element) {
        var node = root;
        for (int i = 0; i < size; i++) {
            if (node.value == element){
                return i;
            } else {
                node = node.next;
            }
        }
        return -1;
    }

    @Override
    public boolean remove(E var1) {
        var node = root;
        for (int i = 0; i < size; i++) {
            if (node.value == var1){
                remove(i);
                return true;
            } else{
                node = node.next;
            }
        }
        return false;
    }

    @Override
    public boolean contains(E var1) {
        return indexOf(var1) != -1;
    }
}