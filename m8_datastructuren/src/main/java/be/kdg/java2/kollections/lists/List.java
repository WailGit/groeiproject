package be.kdg.java2.kollections.lists;/*
 * Wail Azoukane
 *2/12/2022
 *
 */

import be.kdg.java2.kollections.Collection;

public interface List<E> extends Collection<E> {

    void add(int index, E element);
    void add(E element);
    void set(int index, E element);
    int size();
    E remove(int index);
    E get(int index);
    int indexOf(E element);
}
