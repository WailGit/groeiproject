package be.kdg.java2.kollections.maps;/*
 * Wail Azoukane
 *10/12/2022
 *
 */

import be.kdg.java2.kollections.lists.ArrayList;
import be.kdg.java2.kollections.lists.List;
import be.kdg.java2.kollections.sets.ArraySet;
import be.kdg.java2.kollections.sets.Set;

public class HashMap<K, V> implements Map<K, V>{

    private static final int DEFAULT_CAPACITY = 100;

    static class Node<K, V> {
        K key;
        V value;
        Node<K, V> next;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private Node<K, V>[] buckets;
    private int size = 0;

    public HashMap() {
        this(DEFAULT_CAPACITY);
    }

    public HashMap(int capacity) {
        buckets = new Node[capacity];
    }

    private int hash(K key) {
        return Math.abs(key.hashCode() % buckets.length);
    }

    @Override
    public void put(K key, V value) {

        Node<K, V> newNode = new Node<>(key,value);
        if(buckets[hash(key)]!=null){
            newNode.next=buckets[hash(key)];
            buckets[hash(key)]=newNode;
            size++;
        }
        else{
            buckets[hash(key)]=newNode;
            size++;
        }


    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public V get(K key) {
        Node<K, V> node = buckets[hash(key)];

        while(node!=null){
            if(node.key.equals(key)){
                return node.value;
            }
            else{
                node=node.next;
            }
        }
        return null;
    }

    @Override
    public List<V> values() {
        List<V> values = new ArrayList<>();
        for (Node<K, V> bucket : buckets) {
            Node<K, V> node = bucket;
            while (node != null) {
                values.add(node.value);
                node = node.next;
            }
        }
        return values;
    }

    @Override
    public Set<K> keySet() {
        Set<K> keySet = new ArraySet<>();
        for (Node<K, V> bucket : buckets) {
            Node<K, V> node = bucket;
            while (node != null) {
                keySet.add(node.key);
                node = node.next;
            }
        }
        return keySet;
    }
}
