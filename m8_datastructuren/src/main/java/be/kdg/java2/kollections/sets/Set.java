package be.kdg.java2.kollections.sets;/*
 * Wail Azoukane
 *10/12/2022
 *
 */

import be.kdg.java2.kollections.Collection;
import be.kdg.java2.kollections.lists.List;

public interface Set<E> extends Collection<E> {
    List<E> toList();
}

