package be.kdg.java2.kollections.sets;/*
 * Wail Azoukane
 *10/12/2022
 *
 */

import be.kdg.java2.kollections.lists.List;

import java.util.function.Consumer;

public class TreeSet<V extends Comparable<V>> implements Set<V>{

    private static class TreeNode<V extends Comparable<V>> {
        private V value;
        private TreeSet.TreeNode<V> left;
        private TreeSet.TreeNode<V> right;

        public TreeNode(V value) {
            this.value = value;
        }
    }
    private TreeSet.TreeNode<V> root;

    private int size=0;

    @Override
    public List<V> toList() {
        return null;
    }
    @Override
    public boolean remove(V element) {
        size--;
        remove(root,element);





        return false;

    }

    @Override
    public boolean contains(V element) {
        return contains(root,element);
    }

    private boolean contains(TreeNode<V> node, V element){
        if(node==null){
            return false;
        }
        else if (node.value.equals(element)){
            return true;
        }
        return contains(node.left,element) || contains(node.right,element);
    }

    private boolean remove(TreeNode<V> node, V element){

        if(node.value==null) return false;
        if(node.value==element){
            TreeNode<V> lowest= node.right;
            if(lowest!=null){
                while(lowest.left!=null){
                    lowest=lowest.right;
                }
            }else{
                lowest=node.left;
            }
            node.value=lowest.value;
            lowest.value=null;
            size--;
            return true;
        }
        else if(element.compareTo(node.value)<0){
            return remove(node.left,element);
        }
        else{
            return remove(node.right,element);
        }
    }


    @Override
    public int size() {
        return size;
    }



    public void add(V value) {

        if (this.root == null) {
            this.root = new TreeSet.TreeNode<V>(value);
            size++;
        } else {

                add(root, value);
                size++;

        }
    }


    private void add(TreeSet.TreeNode<V> parent, V value) {
        if (value.compareTo(parent.value) < 0) {
            if (parent.left == null) {
                parent.left = new TreeNode<>(value);
                size++;
            } else {
                add(parent.left, value);
            }
        } else if (value.compareTo(parent.value) > 0) {
            if (parent.right == null) {
                parent.right = new TreeNode<>(value);
                size++;
            } else {
                add(parent.right, value);
            }
        }
    }

    public void traverseInOrder(Consumer<V> consumer) {
        traverseInOrder(consumer, root);
    }

    private void traverseInOrder(Consumer<V> consumer,TreeSet.TreeNode<V> node) {
        if (node.left != null) {
            traverseInOrder(consumer, node.left);
        }
        consumer.accept(node.value);
        if (node.right != null) {
            traverseInOrder(consumer, node.right);
        }
    }
}
