package be.kdg.java2.model;

import java.time.LocalDate;
import java.util.*;

public class HistoricFigureFactory {


    private HistoricFigureFactory(){

    }


    public static HistoricFigure newEmptyHistoricFigure() throws IllegalAccessException {
        return new HistoricFigure();
    }

    public static HistoricFigure newFilledHistoricFigure(String name, String country, double height, int kills, GENDER gender, LocalDate birthdate) throws IllegalAccessException {
        return new HistoricFigure(name,country,height,kills,gender,birthdate);
    }

    public static HistoricFigure newRandomHistoricFigure() throws IllegalAccessException {

        Random random = new Random();
        double height=random.nextDouble(0,3);
        int kills = random.nextInt(0,999999999);
        int gender = random.nextInt(0,2);
        GENDER genderOfficial = GENDER.values()[gender];
        String name =HistoricFigureFactory.generateString(10,2,true);
        String country=HistoricFigureFactory.generateString(9,1,false);
        int year = random.nextInt(0,2023);
        int month= random.nextInt(1,13);
        int day;


        if(month==2 ){
            if( isLeapYear(year)){
                day= random.nextInt(1,30);

            }else{
                day= random.nextInt(1,29);

            }
        }
        else{
            day= random.nextInt(1,31);
        }


        LocalDate birthdate = LocalDate.of(year,month,day);

        return  new HistoricFigure(name,country,height,kills,genderOfficial,birthdate);


    }


    public static boolean isLeapYear(int year){

        if(year%4==0){

            if(year%100!=0){
                return true;
            }
            else{
                return year % 400 == 0;
            }

        }
        else{
            return false;
        }





    }

    private static String generateString(int maxWordLength, int wordCount,boolean camelCase){
        String [] klinkers= new String[] {"i", "e", "y", "ø", "u", "o", "a"};
        String [] medeklinkers= new String[] {"b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z"};

        Random random = new Random();
        StringBuilder builder= new StringBuilder();
        maxWordLength=random.nextInt(1,maxWordLength+1);
        int wordLength;
        String letter;
        for (int i = 0; i < wordCount; i++) {
            wordLength = random.nextInt(maxWordLength)+1;
            for (int j = 0; j < wordLength; j++) {
                if (random.nextInt(4) == 0){
                    letter = klinkers[random.nextInt(klinkers.length-1)];
                }else {
                    letter = medeklinkers[random.nextInt(medeklinkers.length-1)];
                }
                if (i > 0 && j ==0 && camelCase){
                    letter = letter.toUpperCase();
                }
                builder.append(letter);
            }
            if (i + 1 < wordCount && !camelCase){
                builder.append(" ");
            }
        }

        return builder.toString();
    }
}
