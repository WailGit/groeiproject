import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigureFactory;
import be.kdg.java2.threading.HistoricFigureAttacker;
import be.kdg.java2.threading.HistoricFigureRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo_10 {


    public static void main(String[] args) throws Exception{
        long totalTime=0;

        List<HistoricFigure> figures = Stream.generate(HistoricFigureFactory::newRandomHistoricFigure).limit(1000).collect(Collectors.toList());


        HistoricFigureAttacker first=new HistoricFigureAttacker(figures,fig -> fig.getGender()== GENDER.Female);
        HistoricFigureAttacker second=new HistoricFigureAttacker(figures,fig -> fig.getKills()>100);
        HistoricFigureAttacker third=new HistoricFigureAttacker(figures,fig ->fig.getHeight()>1.78);

        List<Thread> threads = List.of(
                new Thread(first, "females"),
                new Thread(second, "kills higher than 100"),
                new Thread(third, "heights larger than 1.78")
        );

        threads.forEach(thread -> thread.setDaemon(true));

        // Threads starten
        threads.forEach(Thread::start);
        try {
            for (Thread thread : threads) {
                thread.join();
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Na uitzuivering");

        System.out.printf("aantal vrouwen: %d\n",figures.stream().filter(fig ->fig.getGender()==GENDER.Female).count());
        System.out.printf("aantal lange figures (>1.88): %d\n",figures.stream().filter(fig -> fig.getHeight()>1.88).count());
        System.out.printf("aantal figures die moorden hebben gepleegd: %d\n",figures.stream().filter(fig -> fig.getKills()!=0).count());






    }


}
