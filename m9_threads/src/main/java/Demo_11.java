import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.threading.HistoricFigureCallable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Demo_11 {

    private static final int TEST_COUNT =100;

    public static void main(String[] args) throws Exception{
        ExecutorService executor = Executors.newFixedThreadPool(3);
        long start;
        List<Long> threadTime = new ArrayList<>();


        for(int i = 0; i< TEST_COUNT; i++){
            long startTime = System.currentTimeMillis();
            HistoricFigureCallable call1 = new HistoricFigureCallable(fig -> fig.getGender()== GENDER.Male);
            HistoricFigureCallable call2 = new HistoricFigureCallable(fig -> fig.getKills()==0);
            HistoricFigureCallable call3 = new HistoricFigureCallable(fig -> fig.getHeight()<1.88);

            Future<List<HistoricFigure>> first = executor.submit(call1);
            Future<List<HistoricFigure>> second = executor.submit(call2);
            Future<List<HistoricFigure>> third = executor.submit(call3);
            while (!first.isDone() && !second.isDone() && !third.isDone()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            try {
                executor.awaitTermination(0, TimeUnit.MINUTES);
                threadTime.add(System.currentTimeMillis() - startTime);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        executor.shutdown();
        System.out.printf("3 futures verzamelen elk 1000 figures (gemiddelde uit %d runs): %.2f ms", TEST_COUNT, threadTime.stream().mapToDouble(Long::doubleValue).average().getAsDouble());

    }

        }


