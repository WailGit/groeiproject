import be.kdg.java2.model.HistoricFigureFactory;
import be.kdg.java2.model.HistoricFigures;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo_12 {
    public static void main(String[] args) throws IllegalAccessException {


        HistoricFigures figures = new HistoricFigures();

        List<Thread> threads = Stream.generate(() -> new Thread(() -> Stream.generate(HistoricFigureFactory::newRandomHistoricFigure).limit(5000).forEach(figures::add))).limit(2).collect(Collectors.toList());
        threads.forEach(thread -> {
            thread.setDaemon(true);
            thread.start();
        });
        try{
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Na toevoegen door 2 threads met elk 5000 objecten: figures = " + figures.getSize());

    }
}
