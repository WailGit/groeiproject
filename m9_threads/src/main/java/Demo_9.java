import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.threading.HistoricFigureRunnable;

import java.util.ArrayList;
import java.util.List;

public class Demo_9 {


    private final int TESTCOUNT=100;

    public static void main(String[] args) throws Exception{

        long start;
        List<Long> threadTimes = new ArrayList<>();

        for(int i =0;i<100;i++){
            HistoricFigureRunnable first=new HistoricFigureRunnable(fig -> fig.getGender()== GENDER.Female);
            HistoricFigureRunnable second=new HistoricFigureRunnable(fig -> fig.getKills()>100);
            HistoricFigureRunnable third=new HistoricFigureRunnable(fig ->fig.getHeight()>1.78);
            List<Thread> threads = List.of(
                    new Thread(first, "females"),
                    new Thread(second, "kills higher than 100"),
                    new Thread(third, "heights larger than 1.78")
            );


            threads.forEach(thread -> thread.setDaemon(true));

            // Threads starten
            threads.forEach(Thread::start);

            long startFirst = System.currentTimeMillis();
            try {
                for (Thread thread : threads) {
                    thread.join();
                }

                first.getFigures().stream().limit(5).forEach(System.out::println);
                second.getFigures().stream().limit(5).forEach(System.out::println);
                third.getFigures().stream().limit(5).forEach(System.out::println);

                threadTimes.add(System.currentTimeMillis() - startFirst);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }




        System.out.println(String.format("3 threads verzamelen elk 1000 historic figures (gemiddelde uit 100 runs): %.2f ms",threadTimes.stream().mapToDouble(Long::doubleValue).average().getAsDouble()));


    }
}