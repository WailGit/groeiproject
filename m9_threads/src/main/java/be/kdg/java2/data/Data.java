package be.kdg.java2.data;

import be.kdg.java2.model.GENDER;
import be.kdg.java2.model.HistoricFigure;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Data {

    public static List<HistoricFigure> getData() throws IllegalAccessException {
        List<HistoricFigure> figures= new ArrayList<>();


        figures.add(new HistoricFigure("Jack The Ripper","Engeland",1,16, GENDER.Male, LocalDate.of(1975,1,1)));
        figures.add(new HistoricFigure("Adolf Hitler","Germany",1.75,20946000, GENDER.Male, LocalDate.of(1889,4,20)));
        figures.add(new HistoricFigure("Muhammed Ali","America",1.91,0, GENDER.Male, LocalDate.of(1942,1,17)));
        figures.add(new HistoricFigure("Miyamoto Musashi","Japan",1.84,60, GENDER.Male, LocalDate.of(1584,1,1)));
        figures.add(new HistoricFigure("Napoleon","France",1.68,5000000, GENDER.Male, LocalDate.of(1769,8,15)));


        figures.add(new HistoricFigure("Ramses 2","Egypt",1.75,100, GENDER.Male, LocalDate.of(-1303,1,1)));
        figures.add(new HistoricFigure("Julius Caesar","Italie",1.70,2000000, GENDER.Male, LocalDate.of(-100,7,12)));
        figures.add(new HistoricFigure("Nikola Tesla","America",1.88,0, GENDER.Male, LocalDate.of(1856,7,10)));
        figures.add(new HistoricFigure("Elizabeth 2","Engeland",1.62,1000000, GENDER.Female, LocalDate.of(1926,4,21)));
        figures.add(new HistoricFigure("Blackbeard","Engeland",1.82,0, GENDER.Male, LocalDate.of(1680,1,1)));

        figures.add(new HistoricFigure("Isaac Newton","Spain",1.68,0, GENDER.Male, LocalDate.of(1643,1,4)));
        figures.add(new HistoricFigure("Lü Bu","China",2.08,34000000, GENDER.Male, LocalDate.of(160,1,1)));
        figures.add(new HistoricFigure("Martin Luther King","America",1.69,0, GENDER.Male, LocalDate.of(1929,1,15)));
        figures.add(new HistoricFigure("Albert Einstein","America",1.7,0, GENDER.Male, LocalDate.of(1879,3,14)));
        figures.add(new HistoricFigure("Osama Bin Laden","Pakistan",1.95,3000, GENDER.Male, LocalDate.of(1957,3,10)));




        return figures;

    }
}
