package be.kdg.java2.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import static be.kdg.java2.model.GENDER.UNKOWN;


/*
* DummyWaarde voor datum 1/1/1975
* DummyWaarde voor lengte: 1
* */

public final class HistoricFigure implements Comparable<HistoricFigure> {
    private final String name;
    private final String country;
    private final double height;
    private final int kills;
    private final GENDER gender;
    private final LocalDate birthdate;

    public HistoricFigure() throws IllegalAccessException {
        this("Unkown","Unkown",1,0,UNKOWN,LocalDate.of(1975,1,1));
    }


    public HistoricFigure(String name, String country, double height, int kills, GENDER gender, LocalDate birthdate) {
        this.name = name;
        this.country = country;
        this.height = height;
        this.kills = kills;
        this.gender = gender;
        this.birthdate = birthdate;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public double getHeight() {
        return height;
    }

    public int getKills() {
        return kills;
    }

    public GENDER getGender() {
        return gender;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoricFigure figure = (HistoricFigure) o;

        return name.equals(figure.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public int compareTo(HistoricFigure o) {
        return this.name.compareTo(o.getName());
    }


    @Override
    public String toString() {

        DateTimeFormatter format= DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);

        return String.format("name: %s country:%s height:%.2f kills:%d gender:%s birthdate:%s\n",name,country,height,kills,gender.toString(),format.format(birthdate));

    }
}
