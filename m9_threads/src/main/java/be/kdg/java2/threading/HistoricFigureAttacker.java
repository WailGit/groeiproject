package be.kdg.java2.threading;/*
 * Wail Azoukane
 *13/12/2022
 *
 */

import be.kdg.java2.model.HistoricFigure;

import java.util.List;
import java.util.function.Predicate;

public class HistoricFigureAttacker implements Runnable{

    private List<HistoricFigure> figures;
    private Predicate<HistoricFigure> predicate;


    @Override
    public void run() {
        synchronized (figures){
            figures.removeIf(predicate);
        }


    }

    public HistoricFigureAttacker(List<HistoricFigure> figures, Predicate<HistoricFigure> predicate) {
       this.figures=figures;
        this.predicate=predicate;
    }
}
