package be.kdg.java2.threading;/*
 * Wail Azoukane
 *24/12/2022
 *
 */

import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigureFactory;

import java.util.concurrent.Callable;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HistoricFigureCallable implements Callable {

    private final Predicate<HistoricFigure> filter;




    public HistoricFigureCallable(Predicate<HistoricFigure> filter){
        this.filter=filter;
    }


    @Override
    public Object call() throws Exception {
        return Stream.generate(HistoricFigureFactory::newRandomHistoricFigure).filter(filter).limit(1000).collect(Collectors.toList());
    }
}
