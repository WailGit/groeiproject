package be.kdg.java2.threading;/*
 * Wail Azoukane
 *13/12/2022
 *
 */

import be.kdg.java2.model.HistoricFigure;
import be.kdg.java2.model.HistoricFigureFactory;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HistoricFigureRunnable implements Runnable{

    private final Predicate<HistoricFigure> filter;
    private List<HistoricFigure> figures ;

    @Override
    public void run() {

        figures = Stream.generate(HistoricFigureFactory::newRandomHistoricFigure).filter(filter).limit(1000).collect(Collectors.toList());
    }



    public HistoricFigureRunnable(Predicate<HistoricFigure> filter){
        this.filter=filter;
    }

    public List<HistoricFigure> getFigures() {
        return figures;
    }
}
